
var log = console.log.bind(console),
    fs = require('fs'),
    path = require('path'),
    jsdiff = require('diff'),
    tpl = require('_/micro-templating'),
    Promise = require('promise'),
    utils   = require('_/parser/utils.js'),
    parser   = require('_/parser'),
    orm      = require('orm');
var regexps = require('_/parser/regexps.js');

String.prototype.matchIndex = function(re){
    var res  = [];
    var subs = this.match(re),
        nextIndex, currentIndex;

    for (var cursor = subs.index, l = subs.length, i = 1; i < l; i++){
        var index = cursor;

        if (i+1 !== l && subs[i] !== subs[i+1]) {
            nextIndex = this.indexOf(subs[i+1], cursor);
            while (true) {
                currentIndex = this.indexOf(subs[i], index);
                if (currentIndex !== -1 && currentIndex <= nextIndex)
                    index = currentIndex + 1;
                else
                    break;
            }
            index--;
        } else {
            index = this.indexOf(subs[i], cursor);
        }
        cursor = index + subs[i].length;

        res.push([subs[i], index]);
    }
    return res;
};


module.exports = function (settings) {

    this.gen_path = settings.gen_path;
    this.tpl_path = settings.tpl_path;
    this.models =   settings.models;

    this.syncFiles = function (files_parser, done) {

        var file_parser,
            File = this.models.file,
            _this = this,
            files_promises = [];

        for (var i in files_parser) {
            file_parser = files_parser[i];



            // if @sync is found
            if (file_parser.tags.indexOf("@prop") >= 0) {

                function wrap_files_promises(file_parser) {
                    return new Promise(function (fulfill, reject) {
                        File.find({path: file_parser.path, tags: orm.like("%@prop%")}, function (err, files) {
                            if (err) throw err;

                            var syncfiles_promises = [];
                            var lang = file_parser.path.substr(file_parser.path.lastIndexOf(".") + 1),
                                classes_tree = parser.code.analyse(file_parser.body, lang);

                            for (var l=0; l<files.length; l++) {
                                var file = files[l];


                                if (classes_tree.length === 0) {

                                    function wrap_syncfiles_promises(file) {
                                        return new Promise(function (fulfill, reject) {
                                            _this.syncFile(file_parser, file, function () {
                                                // done
                                                fulfill();
                                            });
                                        })
                                    }

                                    // just update file (defined by partial keyword) with file_parser.body
                                    syncfiles_promises.push(wrap_syncfiles_promises(file));

                                } else {

                                    function wrap_syncfiles_promises2(file) {
                                        return new Promise(function (fulfill, reject) {
                                            file.getCls(function (err, cls) {
                                                if (err) throw err;

                                                cls.getFragment(function (err, fragments) {
                                                    if (err) throw err;

                                                    var syncfragment_promises = [];

                                                    // find whether there is a fragment that needs to be updated in the sources
                                                    for (var k in fragments) {
                                                        var fragment = fragments[k], fragment_matched = false;

                                                        // match class
                                                        for (var j in classes_tree) {
                                                            // compare signatures of classes (1st lvl)
                                                            if (classes_tree[j].signature == fragment.signature) {

                                                                // childs (methods/attributes) of changed parsed file
                                                                var attr_methods = classes_tree[j].childs;
                                                                // childs (methods/attributes) of fragment from database
                                                                var childs = JSON.parse(fragment.parser).childs;
                                                                for (var l in attr_methods) {
                                                                    for (var m in childs) {
                                                                        if (attr_methods[l].signature == childs[m].signature) {


                                                                            // match

                                                                            function wrap_syncfragment_promises(fragment, child) {
                                                                                return new Promise(function (fulfill, reject) {
                                                                                    // update fragment partial body (an partial file) with file_parser.body
                                                                                    _this.syncFragment(file_parser, file, {
                                                                                        fragment: fragment,
                                                                                        cls: cls,
                                                                                        child: child
                                                                                    }, function () {
                                                                                        // done
                                                                                        fulfill();
                                                                                    });
                                                                                });
                                                                            }

                                                                            syncfragment_promises.push(wrap_syncfragment_promises(fragment, childs[m]));
                                                                            fragment_matched = true;
                                                                            if (fragment_matched) break;
                                                                        }
                                                                    }

                                                                    if (fragment_matched) break;
                                                                }

                                                                if (fragment_matched) break;
                                                            }
                                                        }
                                                    }

                                                    Promise.all(syncfragment_promises).then(function () {
                                                        fulfill();
                                                    });

                                                });

                                            });
                                        })
                                    }


                                    syncfiles_promises.push(wrap_syncfiles_promises2(file));
                                }

                            }

                            Promise.all(syncfiles_promises).then(function () {
                                fulfill();
                            });

                        });

                    });
                }

                files_promises.push(wrap_files_promises(file_parser));

            }


        }

        Promise.all(files_promises).then(function () {
            done();
        });

    };


    this.syncFragment = function (file_parser, file, fragment, done) {

        var fragment_cls = null,
            child = null;
        if (!fragment)
            fragment = null;
        else {
            fragment_cls = fragment.cls;
            child = fragment.child;
            fragment = fragment.fragment;
        }

        var _this = this,
            Usecase = this.models.usecase,
            UsecaseFile = this.models.usecase_file;


        // I think it is enough to update just sources, because they will trigger use case update
        file.body = file_parser.body;
        file.save(function (err) {
            if (err) throw err;


            // search for use cases to update which reference to multiple use cases path based on file
            UsecaseFile.find({file_id: file.id}, function (err, usecases) {
                if (err) throw err;


                var files_promises = [];

                for (var i in usecases) {

                    var usecase = usecases[i].usecase;

                    function wrap_pass_usecase(usecase) {
                        return new Promise(function (fulfill, reject) {
                            fs.readFile(usecase.path, {encoding: "UTF-8"}, function (err, data) {
                                if (err) throw err;


                                var promises = [];

                                var match = data.matchIndex(regexps.comments());
                                //console.log(match, file_parser.path);
                                for (var i in match) {
                                    var substr = match[i][0],
                                        start = match[i][1],
                                        len = substr.length,
                                        end = start + len,
                                        pathPos = substr.indexOf(file_parser.path),
                                        syncPos = substr.indexOf("@prop");

                                    // find path in first 100 characters
                                    if ( (pathPos > -1 && pathPos < 100) &&

                                            // and must contain @sync flag
                                        (syncPos > -1 && syncPos < 100) &&

                                            // and if there is a fragment, check the cls name and signature (whether it contains it)
                                        (
                                             fragment === null ||
                                            (fragment !== null &&
                                                      substr.indexOf(fragment_cls.name) != -1 &&
                                                      // class
                                                      substr.indexOf(fragment.signature) != -1 &&
                                                      // child, method or attribute
                                                      substr.indexOf(child.signature) != -1)
                                        )

                                    ) {

                                        //if (fragment) {
                                        //    console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', fragment.signature, usecase.name);
                                        //    done();return;
                                        //}
                                        // update

                                        // skip already the same
                                        if (file_parser.raw_body == substr) continue;

                                        //var
                                        var p1 = data.substring(0, start),
                                            p2 = data.substring(end, data.length),
                                            body = p1 + file_parser.raw_body + p2;

                                        promises.push(
                                            new Promise(function (fulfill, reject) {
                                                fs.writeFile(usecase.path, body, function (err) {
                                                    if (err) throw err;
                                                    fulfill();
                                                });
                                            })
                                        );


                                    }

                                }

                                Promise.all(promises).then(function () {
                                    fulfill();
                                });

                            });
                        });
                    }
                    files_promises.push(wrap_pass_usecase(usecase));
                }

                Promise.all(files_promises).then(function () {
                    done();
                });
            });
        });
    };
    this.syncFile = function (file_parser, file, done) {

        this.syncFragment(file_parser, file, null, function () {
            done();
        });

    };

};