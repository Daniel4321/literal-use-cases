
var regexps = require('_/parser/regexps');

module.exports = function (orm, db) {
    var Fragment = db.define('fragment', {
            type                : { type: 'text', required: true },
            name                : { type: 'text', required: true },
            lang                : { type: 'text', required: true },
            signature           : { type: 'text', required: true },
            /**
             * imports and such
             */
            before_body         : { type: 'text', required: true },
            body                : { type: 'text', required: true },
            after_body          : { type: 'text', required: true },
            file_body           : { type: 'text', required: true },
            parser              : { type: 'text', required: true },
            created_at      : { type: 'date', required: true, time: true }
        },
        {
            //autoFetch : true,
            //cache : false,

            hooks: {
                beforeValidation: function () {
                    this.created_at = new Date();
                }

            },
            methods: {
                construct: function (scope_override) {

                    var def = this.signature,
                        scope = scope_override !== undefined ? scope_override : (this.body !== undefined ? this.body : '');

                    return regexps[this.lang].separator +
                        def +
                        scope;

                }
            }
        });

    Fragment.hasOne('usecase', db.models.usecase, { autoFetch : true  });
    //Fragment.hasOne('usecase', db.models.usecase);
    //Fragment.hasMany('fragment', db.models.fragment, {}, { key: true, autoFetch : true  });

    Fragment.constructClsWithFragments = function (fragments) {

        var _this = fragments[0];

        if (_this.type === 'cls') {
            var fragments_bodies = "",
                fragment_body,
                fragments_to_merge = [],
                signatures_to_merge = [];



            for (var i in fragments) {

                if (fragments[i].type === 'cls' && _this.name === fragments[i].name) {
                    var childs = JSON.parse(fragments[i].parser).childs;
                    for (var j in childs) {
                        var child = childs[j],
                            fragment = new Fragment();

                        fragment.type = child.type;
                        fragment.name = child.name;
                        fragment.lang = child.lang;
                        fragment.signature = child.signature;
                        fragment.body = child.body;
                        fragment.usecase_id = fragments[i].usecase.id;
                        fragment.usecase = fragments[i].usecase;
                        fragment.parser = JSON.stringify(child);

                        fragments_to_merge.push(fragment);
                    }

                }
                if (fragments[i].type === 'attr') {
                    fragments_to_merge.push(fragments[i]);
                }
                if (fragments[i].type === 'method') {
                    fragments_to_merge.push(fragments[i]);
                }
            }

            var last_uc_id = 0, uc_id = 0,
                marker = "";

            for (var i in fragments_to_merge) {

                if (fragments_to_merge[i].type === 'attr' || fragments_to_merge[i].type === 'method') {
                    fragment_body = fragments_to_merge[i].construct();
                    //console.log(fragments_to_merge[i].usecase);
                    //console.log(fragments_to_merge[i].type+']'+this.usecase.name+'['+fragments_to_merge[i].name+fragments_to_merge[i].usecase);
                    uc_id = fragments_to_merge[i].usecase.id;

                    /**
                     * if no signature
                     */
                    if (signatures_to_merge.indexOf(fragments_to_merge[i].signature) === -1) {

                        if (uc_id !== last_uc_id) {
                            var replacements = {
                                "%usecase%":    "Fragment "+fragments_to_merge[i].usecase.name
                            };

                            marker = regexps[_this.lang].mark_fragment.replace(/%\w+%/g, function(all) {
                                return replacements[all] || all;
                            });
                            marker = regexps[_this.lang].separator +
                                marker ;

                        }

                        fragments_bodies += marker +
                            regexps[_this.lang].separator+
                            fragment_body;

                        signatures_to_merge.push(fragments_to_merge[i].signature);

                    }

                    last_uc_id = fragments_to_merge[i].usecase.id;
                    marker = "";
                }
            }
            return _this.construct(
                regexps[_this.lang].separator +
                fragments_bodies +
                regexps[_this.lang].separator +
                regexps[_this.lang].scope_end_cls

            );
        }
        return null;
    };

    Fragment.constructOutsideBody = function (key, fragments) {

        var _this = fragments[0],
            body = "",
            signatures_to_merge = [];

        for (var i in fragments) {
            var fragment = fragments[i],
                before_body = JSON.parse(fragment[key]);

            for (var j in before_body) {

                var el = new Fragment(),
                    elF = before_body[j];

                /**
                 * if no signature
                 */
                if (signatures_to_merge.indexOf(elF.signature) === -1) {

                    el.type = elF.type;
                    el.name = elF.name;
                    el.lang = elF.lang;
                    el.signature = elF.signature;
                    el.body = elF.body;
                    el.file_body = elF.file_body;

                    body +=
                        el.construct();

                    signatures_to_merge.push(elF.signature);
                }

            }
            body += regexps[_this.lang].separator;
        }
        return body;
    };

    return Fragment;
};