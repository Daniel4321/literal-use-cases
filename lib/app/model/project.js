
var log = console.log.bind(console),
    Promise = require('promise');

module.exports = function (orm, db) {
    var Project = db.define('project', {
            name                : { type: 'text', required: true },
            user                : { type: 'text', required: true },
            project_path        : { type: 'text', required: true },
            migration_path      : { type: 'text', required: true },
            tpl_path            : { type: 'text', required: true },
            gen_path            : { type: 'text', required: true }
        },
        {
            //autoFetch : true,
            //cache : false,

            hooks: {
                beforeValidation: function () {
                    this.created_at = new Date();
                }
            },
            methods: {},
        });
    return Project;
};