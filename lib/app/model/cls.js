
var log = console.log.bind(console),
    Promise = require('promise');

module.exports = function (orm, db) {
    var Class = db.define('cls', {
            name            : { type: 'text', required: true },
        },
        {
            hooks: {

            },
            methods: {

            }
        });

    Class.hasMany('fragment', db.models.fragment, {  }, { cascadeRemove: true, key: true, autoFetch : true });

    Class.set_fragments = function (usecase, create, update, done) {
        var changes = [];
        for (var i in create) {

            function do_create(cls, fragments) {

                return new Promise(function (fulfill, reject) {
                    cls.addFragment(fragments, function (err) {
                        if (err) throw err;

                        fulfill();
                    });
                });
            }

            changes.push(do_create(create[i].cls, create[i].fragments));
        }


        for (var i in update) {

            function do_update(cls_id, fragments) {

                return new Promise(function (fulfill, reject) {
                    Class.get(cls_id, function (err, cls) {
                        if (err) throw err;

                        cls.setFragment(fragments, function (err) {
                            if (err) throw err;

                            fulfill();
                        });
                    })
                });
            }

            changes.push(do_update(i, update[i]));
        }

        Promise.all(changes).then(function () {

            done();
        });

    };
    return Class;
};