module.exports = function (orm, db) {
    var Scenario = db.define('scenario', {

        },
        {
            cascadeRemove: true,
            hooks: {

            },
            methods: {

            }
        });

    Scenario.hasMany('step', db.models.step, {}, { cascadeRemove: true, key: true });

    return Scenario;

};