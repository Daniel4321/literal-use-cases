
var log = console.log.bind(console),
    Promise = require('promise');

module.exports = function (orm, db) {
    var Usecase = db.define('usecase', {
            name            : { type: 'text', required: true },
            level           : { type: 'text' },
            path            : { type: 'text', required: true },
            description     : { type: 'text' },
            specializes     : { type: 'text' },
            parser          : { type: 'text' },
            created_at      : { type: 'date', required: true, time: true }
        },
        {
            //autoFetch : true,
            //cache : false,

            hooks: {
                beforeValidation: function () {
                    this.created_at = new Date();
                }
            },
            methods: {
                cascadeRemove: function (done) {

                    var usecase = this,
                        Argument = db.models.argument,
                        Cls = db.models.cls,
                        Fragment = db.models.fragment,
                        Step = db.models.step,
                        File = db.models.file,
                        Actor = db.models.actor,
                        ExtensionPoint = db.models.extension_point,
                        Relationship = db.models.relationship,
                        remove = {
                            usecase: null,
                            actors: [],
                            extension_points: [],
                            scenario: null,
                            steps: [],
                            arguments: [],
                            files: [],
                            cls: [],
                            fragments: []
                        };

                    /**
                     * Fill the remove array
                     */
                    remove.usecase = usecase;

                    var l1 = new Promise(function (fulfill, reject) {
                        usecase.getActor(function (err, actors) {
                            if (err) throw err;
                            remove.actors = actors;
                            fulfill();
                        });
                    });

                    var l2 = new Promise(function (fulfill, reject) {
                        usecase.getExtensionPoint(function (err, extension_points) {
                            if (err) throw err;
                            remove.extension_points = extension_points;
                            fulfill();
                        });
                    });


                    var l3 = new Promise(function (fulfill, reject) {
                        usecase.getScenario(function (err, scenario) {
                            if (err) throw err;
                            scenario.getStep().each(function (step) {
                                remove.steps.push(step);
                                remove.arguments = remove.arguments.concat(step.argument);
                            }).save(function (err) {
                                if (err) throw err;
                                remove.scenario = scenario;
                                fulfill();
                            })
                        });
                    });


                    var l4 = new Promise(function (fulfill, reject) {
                        usecase.getFile(function (err, files) {
                            if (err) throw err;
                            remove.files = files;

                            for (var i in files) {
                                remove.cls.push(files[i].cls);
                            }
                            fulfill();

                        });
                    });


                    var l5 = new Promise(function (fulfill, reject) {
                        Fragment.find({ usecase: usecase }, function (err, fragments) {
                            if (err) throw err;
                            remove.fragments = fragments;
                            fulfill();
                        });
                    });

                    var l6 = new Promise(function (fulfill, reject) {
                        Relationship.removeAll( usecase, function () {
                            fulfill();
                        });
                    });


                    /**
                     * Do fill the remove array
                     */
                    Promise.all([l1, l2, l3, l4, l5, l6]).then(function () {

                        /**
                         * Remove actors
                         */
                        var remove_actors = new Promise(function (fulfill, reject) {
                            remove.usecase.removeActor(remove.actors, function (err) {
                                if (err) throw err;

                                var actor_ids = [];
                                for (var i in remove.actors) actor_ids.push(remove.actors[i].id);

                                Actor.find({id: actor_ids}).remove(function (err) {
                                    if (err) throw err;

                                    fulfill();

                                });

                            });
                        });

                        var remove_extension_points = new Promise(function (fulfill, reject) {
                            remove.usecase.removeExtensionPoint(remove.extension_points, function (err) {
                                if (err) throw err;

                                var extension_point_ids = [];
                                for (var i in remove.extension_points) extension_point_ids.push(remove.extension_points[i].id);

                                ExtensionPoint.find({id: extension_point_ids}).remove(function (err) {
                                    if (err) throw err;

                                    fulfill();

                                });

                            });
                        });


                        var remove_scenario = new Promise(function (fulfill, reject) {

                            /**
                             * Remove steps
                             * related table between steps-arguments and it's items are cascade removed
                             */
                            var step_ids = [];
                            for (var i in remove.steps) step_ids.push(remove.steps[i].id);


                            Step.find({id: step_ids}).each(function (step) {
                                step.argument = [];
                            }).save(function (err) {
                                if (err) throw err;

                                Step.find({id: step_ids}).remove(function (err) {
                                    if (err) throw err;

                                    /**
                                     * Remove arguments
                                     */
                                    var argument_ids = [];
                                    for (var i in remove.arguments) argument_ids.push(remove.arguments[i].id);

                                    Argument.find({id: argument_ids}).remove(function (err) {
                                        if (err) throw err;


                                        /**
                                         * Remove scenario
                                         * cascade remove
                                         */
                                        remove.scenario.setStep([], function (err) {

                                            remove.scenario.remove(function (err) {
                                                if (err) throw err;

                                                fulfill();
                                            });
                                        });


                                    });

                                });
                            });
                        });



                        var remove_files = new Promise(function (fulfill, reject) {


                            /**
                             * Remove classes
                             * cascade remove with fragments
                             */
                            var cls_ids = [];
                            for (var i in remove.cls) cls_ids.push(remove.cls[i].id);

                            Cls.find({id: cls_ids}).each(function (cls) {
                                cls.fragment = [];
                            }).save(function (err) {
                                if (err) throw err;


                                /**
                                 * Remove files
                                 * related table between usecases-files and it's items are cascade removed
                                 */
                                remove.usecase.setFile([], function (err) {
                                    if (err) throw err;


                                    Cls.find({id: cls_ids}).remove(function (err) {
                                        if (err) throw err;


                                        /**
                                         * Remove fragments
                                         */
                                        var fragment_ids = [];
                                        for (var i in remove.fragments) fragment_ids.push(remove.fragments[i].id);

                                        Fragment.find({id: fragment_ids}).remove(function (err) {
                                            if (err) throw err;



                                            var file_ids = [];
                                            for (var i in remove.files) file_ids.push(remove.files[i].id);

                                            File.find({id: file_ids}).remove(function (err) {
                                                if (err) throw err;

                                                fulfill();

                                            });

                                        });
                                    });

                                });

                            });
                        });

                        Promise.all(remove_actors, remove_extension_points, remove_scenario, remove_files).then(function () {

                            /**
                             * Finally, remove usecase
                             */
                            remove.usecase.remove(function (err) {
                                if (err) throw err;

                                done();
                            });
                        });



                    });

                }
            }
        });

    Usecase.hasOne('scenario', db.models.scenario);
    Usecase.hasOne('project', db.models.project);
    Usecase.hasMany('actor', db.models.actor, {}, { key: true });

    Usecase.upsert = function (parser_usecase, project_id, done) {
        var Usecase = db.models.usecase,
            Scenario = db.models.scenario,
            Actor = db.models.actor,
            Step = db.models.step,
            Argument = db.models.argument,
            ExtensionPoint = db.models.extension_point,
            Relationship = db.models.relationship;

        if (parser_usecase === null ||
                parser_usecase.scenario === null ||
                Object.keys(parser_usecase.scenario.steps).length === 0) {
            done(null, null);
            return;
        }

        Usecase.find({ path: parser_usecase.path }, function (err, usecases) {
            if (err) throw err;



            /*
             * INSERT
             */
            if (usecases.length === 0) {



                /*
                 * Create usecase
                 */
                Usecase.create({
                        name:           parser_usecase.name,
                        path:           parser_usecase.path,
                        level:          parser_usecase.level,
                        description:    parser_usecase.description,
                        specializes:    parser_usecase.specializes,
                        project_id:     project_id,
                        parser:    JSON.stringify(parser_usecase)

                }, function (err, usecase) {
                    if (err) throw err;

                    Relationship.upsert(project_id, parser_usecase, usecase, true, function () {

                        /*
                         * Create scenario
                         */
                        Scenario.create({}, function (err, scenario) {
                            if (err) throw err;

                            /*
                             * Remap and prepare actors to insert
                             */
                            var actors = [];
                            for (var actor in parser_usecase.actors) {
                                actors.push({
                                    name:           parser_usecase.actors[actor].name,
                                    description:    parser_usecase.actors[actor].description
                                });
                            }

                            /*
                             * Remap and prepare extension_points to insert
                             */
                            var extension_points = [];
                            for (var i in parser_usecase.extension_points) {
                                extension_points.push({
                                    name:           parser_usecase.extension_points[i].name,
                                    step_no:        parser_usecase.extension_points[i].step_no,
                                    usecase_id:     usecase.id,
                                    project_id:     project_id
                                });
                            }



                            /*
                             * Remap and prepare steps to insert
                             *
                             * step_types:
                             *
                             * 1 = trigger
                             * 2 = step
                             * 3 = condition
                             * 4 = precondition
                             * 5 = postcondition
                             */
                            var steps = [];

                            for (var i in parser_usecase.triggers) {

                                var step = parser_usecase.triggers[i];

                                var arguments = [];
                                for (var i in step.args) {
                                    arguments.push({
                                        value: step.args[i]
                                    });
                                };

                                steps.push({
                                    no:             step.no,
                                    name:           step.name,
                                    played_by:      step.played_by,
                                    inherited:      step.inherited,
                                    step_type_id:   1,
                                    argument:       arguments,
                                    project_id:     project_id,
                                    usecase:        usecase
                                });
                            }

                            for (var step_no in parser_usecase.scenario.steps) {

                                var step = parser_usecase.scenario.steps[step_no];

                                if (step.type === "step") {
                                    var step_type = 2;
                                } else if (step.type === "condition") {
                                    var step_type = 3;
                                }

                                var arguments = [];
                                for (var i in step.args) {
                                    arguments.push({
                                        value: step.args[i]
                                    });
                                };

                                steps.push({
                                    no:             step.no,
                                    name:           step.name,
                                    played_by:      step.played_by,
                                    inherited:      step.inherited,
                                    step_type_id:   step_type,
                                    argument:       arguments,
                                    project_id:     project_id,
                                    usecase:        usecase
                                });
                            }


                            for (var i in parser_usecase.preconditions) {

                                var step = parser_usecase.preconditions[i];

                                var arguments = [];
                                for (var i in step.args) {
                                    arguments.push({
                                        value: step.args[i]
                                    });
                                };

                                steps.push({
                                    no:             step.no,
                                    name:           step.name,
                                    played_by:      step.played_by,
                                    inherited:      step.inherited,
                                    step_type_id:   4,
                                    argument:       arguments,
                                    project_id:     project_id,
                                    usecase:        usecase
                                });
                            }

                            for (var i in parser_usecase.postconditions) {

                                var step_type = 5,
                                    step = parser_usecase.postconditions[i];

                                var arguments = [];
                                for (var i in step.args) {
                                    arguments.push({
                                        value: step.args[i]
                                    });
                                };

                                steps.push({
                                    no:             step.no,
                                    name:           step.name,
                                    played_by:      step.played_by,
                                    inherited:      step.inherited,
                                    step_type_id:   5,
                                    argument:       arguments,
                                    project_id:     project_id,
                                    usecase:        usecase
                                });
                            }

                            /*
                             * Do insert!
                             *
                             * 1. Create actors
                             */
                            Actor.create(actors, function (err, actors) {
                                if (err) throw err;

                                var addExtensionPoints = function (usecase, extension_points) { return new Promise(function (resolve, reject) {
                                    if (extension_points.length === 0) resolve();

                                    /*
                                     * Add extension_points
                                     */
                                    ExtensionPoint.create(extension_points, function (err, extension_points) {
                                        if (err) throw err;


                                        usecase.addExtensionPoint(extension_points, function (err) {
                                            if (err) throw err;

                                            resolve();
                                        });

                                    });
                                })};

                                addExtensionPoints(usecase, extension_points).done(function () {

                                    /*
                                     * Actors are not required
                                     */
                                    if (actors.length > 0) {

                                        /*
                                         * 2. Add actors to usecase
                                         */
                                        usecase.addActor(actors, function (err) {
                                            if (err) throw err;


                                            /*
                                             * 3. Add scenario to usecase
                                             */
                                            usecase.setScenario(scenario, function (err) {
                                                if (err) throw err;


                                                /*
                                                 * 4. Create steps along with arguments
                                                 */
                                                Step.create(steps, function (err, steps) {
                                                    if (err) throw err;

                                                    /*
                                                     * 5. Finally, add steps to scenario
                                                     */
                                                    scenario.addStep(steps, function (err) {
                                                        if (err) throw err;

                                                            done(usecase, parser_usecase);


                                                    });
                                                });
                                            });
                                        });

                                    } else {

                                        /*
                                         * 3. Add scenario to usecase
                                         */
                                        usecase.setScenario(scenario, function (err) {
                                            if (err) throw err;


                                            /*
                                             * 4. Create steps along with arguments
                                             */
                                            Step.create(steps, function (err, steps) {
                                                if (err) throw err;

                                                /*
                                                 * 5. Finally, add steps to scenario
                                                 */
                                                scenario.addStep(steps, function (err) {
                                                    if (err) throw err;

                                                        done(usecase, parser_usecase);

                                                });
                                            });
                                        });
                                    }

                                });

                            });
                        });
                    });
                    
                });


                /*
                 * UPDATE
                 */
            } else if (usecases.length === 1) {
                var usecase = usecases[0];


                Relationship.upsert(project_id, parser_usecase, usecase, false, function () {

                    usecase['parser'] = JSON.stringify(parser_usecase);

                    var attrs = ["name", "level", "path", "description", "specializes"];
                    for (var i in attrs) {
                        if (usecase[attrs[i]] === parser_usecase[attrs[i]])
                            delete parser_usecase[attrs[i]];
                        else
                            usecase[attrs[i]] = parser_usecase[attrs[i]];
                    }

                    usecase.save(function (err) {
                        if (err) throw err;


                        /*
                         * ACTORS
                         */
                        usecase.getActor(function (err, db_actors) {

                            var actors = {
                                remove:   [],
                                create:   [],
                                update:   []
                            };

                            for (var i in db_actors) {
                                var db_actor = db_actors[i],
                                    parser_actor = (db_actor.name in parser_usecase.actors) ? parser_usecase.actors[db_actor.name] : null;

                                if (parser_actor === null) {
                                    actors.remove.push(db_actor);
                                } else {
                                    // update
                                    var attrs = ["name", "description"];
                                    for (var i in attrs) {
                                        if (db_actor[attrs[i]] === parser_actor[attrs[i]])
                                            delete parser_actor[attrs[i]];
                                        else
                                            db_actor[attrs[i]] = parser_actor[attrs[i]];
                                    }

                                    if (Object.keys(parser_actor).length === 0)
                                        delete parser_usecase.actors[db_actor.name];

                                    actors.update.push(db_actor);
                                }
                            }

                            // add new from parser_usecase
                            for (var actor in parser_usecase.actors) {
                                // check if actor in db_actors
                                var skip = false;
                                for (var i in db_actors)
                                    if (db_actors[i].name === actor) {
                                        skip = true;
                                        break;
                                    }
                                if (skip) continue;

                                // add new
                                actors.create.push({
                                    name:           parser_usecase.actors[actor].name,
                                    description:    parser_usecase.actors[actor].description
                                });
                            }

                            if (Object.keys(parser_usecase.actors).length === 0)
                                delete parser_usecase.actors;

                            /*
                             * Extension points
                             */

                            usecase.getExtensionPoint(function (err, db_extension_points) {

                                var extension_points = {
                                    remove:   [],
                                    create:   [],
                                    update:   []
                                };
                                for (var i in db_extension_points) {
                                    var db_extension_point = db_extension_points[i],
                                        parser_extension_point = (db_extension_point.step_no in parser_usecase.extension_points) ? parser_usecase.extension_points[db_extension_point.step_no] : null;

                                    if (parser_extension_point === null) {
                                        extension_points.remove.push(db_extension_point);
                                    } else {
                                        // update
                                        var attrs = ["name", "step_no"];
                                        for (var i in attrs) {
                                            if (db_extension_point[attrs[i]] === parser_extension_point[attrs[i]])
                                                delete parser_extension_point[attrs[i]];
                                            else
                                                db_extension_point[attrs[i]] = parser_extension_point[attrs[i]];
                                        }

                                        if (Object.keys(parser_extension_point).length === 0)
                                            delete parser_usecase.extension_points[db_extension_point.step_no];

                                        extension_points.update.push(db_extension_point);
                                    }
                                }

                                // add new from parser_usecase
                                for (var step_no in parser_usecase.extension_points) {
                                    // check if step_no in db_extension_points
                                    var skip = false;
                                    for (var i in db_extension_points)
                                        if (db_extension_points[i].step_no === step_no) {
                                            skip = true;
                                            break;
                                        }
                                    if (skip) continue;

                                    // add new
                                    extension_points.create.push({
                                        name:           parser_usecase.extension_points[step_no].name,
                                        step_no:        parser_usecase.extension_points[step_no].step_no,
                                        usecase_id:     usecase.id,
                                        project_id:     project_id

                                    });
                                }

                                if (Object.keys(parser_usecase.extension_points).length === 0)
                                    delete parser_usecase.extension_points;


                                /*
                                 * STEPS
                                 */
                                var steps = {
                                    remove:   [],
                                    create:      [],
                                    update:   []
                                },
                                    argument_remove_ids = [-1],
                                    triggers_orig = JSON.parse(JSON.stringify(parser_usecase.triggers)),
                                    steps_orig = JSON.parse(JSON.stringify(parser_usecase.scenario.steps)),
                                    preconditions_orig = JSON.parse(JSON.stringify(parser_usecase.preconditions)),
                                    postconditions_orig = JSON.parse(JSON.stringify(parser_usecase.postconditions));

                                usecase.getScenario(function (err, scenario) {
                                    if (err) throw err;

                                    scenario.getStep(function (err, db_steps) {
                                        if (err) throw err;

                                        for (var i in db_steps) {
                                            var db_step = db_steps[i],
                                                parser_step = null, parser_step_i;

                                            /*
                                             * Find corresponding parser_step in parser_usecase.scenario.steps
                                             *
                                             * step_types:
                                             *
                                             * 1 = trigger
                                             * 2 = step
                                             * 3 = condition
                                             * 4 = precondition
                                             * 5 = postcondition
                                             */
                                            switch (db_step.step_type_id) {
                                                case 1:
                                                    for (var i in parser_usecase.triggers) {
                                                        if (parser_usecase.triggers[i].name === db_step.name) {
                                                            parser_step = parser_usecase.triggers[i];
                                                            parser_step_i = i;
                                                            break;
                                                        }
                                                    }
                                                    break;

                                                case 2:
                                                case 3:
                                                    for (var step_no in parser_usecase.scenario.steps) {
                                                        if (parser_usecase.scenario.steps[step_no].name === db_step.name
                                                                && step_no === db_step.no) {
                                                            parser_step = parser_usecase.scenario.steps[step_no];
                                                            parser_step_i = step_no;
                                                            break;
                                                        }
                                                    }
                                                    break;

                                                case 4:
                                                    for (var i in parser_usecase.preconditions) {
                                                        if (parser_usecase.preconditions[i].name === db_step.name) {
                                                            parser_step = parser_usecase.preconditions[i];
                                                            parser_step_i = i;
                                                            break;
                                                        }
                                                    }
                                                    break;

                                                case 5:
                                                    for (var i in parser_usecase.postconditions) {
                                                        if (parser_usecase.postconditions[i].name === db_step.name) {
                                                            parser_step = parser_usecase.postconditions[i];
                                                            parser_step_i = i;
                                                            break;
                                                        }
                                                    }
                                                    break;

                                            }

                                            if (parser_step === null) {
                                                steps.remove.push(db_step);
                                                for (var i in db_step.argument) {
                                                    argument_remove_ids.push(db_step.argument[i].id);
                                                }
                                            } else {
                                                // update
                                                var attrs = ["no", "name", "orig_name", "played_by", "inherited"];
                                                for (var i in attrs) {
                                                    if (db_step[attrs[i]] === parser_step[attrs[i]])
                                                        delete parser_step[attrs[i]];
                                                    else
                                                        db_step[attrs[i]] = parser_step[attrs[i]];
                                                }

                                                // if arg is not found in db args, add argument
                                                for (var i in parser_step.args) {
                                                    var found = false;
                                                    for (var j in db_step.argument) {
                                                        if (parser_step.args[i] === db_step.argument[j].value) {
                                                            found = true;
                                                            break;
                                                        }
                                                    }
                                                    if (!found) {
                                                        db_step.argument.push({value:parser_step.args[i]});

                                                    }
                                                }

                                                // if arg is found in parser args, nothing to do
                                                for (var i in db_step.argument) {
                                                    var found = false;

                                                    for (var j in parser_step.args) {
                                                        if (parser_step.args[j] === db_step.argument[i].value) {
                                                            delete parser_step.args[j];
                                                            found = true;
                                                            break;
                                                        }
                                                    }

                                                    // if not found in parser_step, then delete it
                                                    if (!found) {
                                                        argument_remove_ids.push(db_step.argument[i].id);
                                                        delete db_step.argument[i];
                                                    }
                                                }


                                                steps.update.push(db_step);




                                                switch (db_step.step_type_id) {
                                                    case 1:
                                                        if (Object.keys(parser_usecase.triggers[parser_step_i]).length === 0)
                                                            delete parser_usecase.triggers[parser_step_i];
                                                        break;

                                                    case 2:
                                                    case 3:
                                                        if (Object.keys(parser_usecase.scenario.steps[parser_step_i]).length === 0)
                                                            delete parser_usecase.scenario.steps[parser_step_i];
                                                        break;

                                                    case 4:
                                                        if (Object.keys(parser_usecase.preconditions[parser_step_i]).length === 0)
                                                            delete parser_usecase.preconditions[parser_step_i];
                                                        break;

                                                    case 5:
                                                        if (Object.keys(parser_usecase.postconditions[parser_step_i]).length === 0)
                                                            delete parser_usecase.postconditions[parser_step_i];
                                                        break;

                                                }

                                                parser_step.args =
                                                    parser_step.args.filter(String);

                                                if (parser_step.args.length === 0)
                                                    delete parser_step.args;

                                                if (Object.keys(parser_step).length === 1 && 'type' in parser_step)
                                                    delete parser_step.type;

                                            }
                                        }

                                        /*
                                         * Remove empty fields to reflect changes
                                         */
                                        for (var i in parser_usecase.triggers) {
                                            if (Object.keys(parser_usecase.triggers[i]).length === 0)
                                                delete parser_usecase.triggers[i];
                                        }
                                        parser_usecase.triggers =
                                            parser_usecase.triggers.filter(Object);
                                        if (parser_usecase.triggers.length === 0)
                                            delete parser_usecase.triggers;

                                        for (var i in parser_usecase.scenario.steps) {
                                            if (Object.keys(parser_usecase.scenario.steps[i]).length === 0)
                                                delete parser_usecase.scenario.steps[i];
                                        }

                                        if (Object.keys(parser_usecase.scenario.steps).length === 0)
                                            delete parser_usecase.scenario.steps;


                                        for (var i in parser_usecase.scenario) {
                                            if (Object.keys(parser_usecase.scenario[i]).length === 0)
                                                delete parser_usecase.scenario[i];
                                        }
                                        if (Object.keys(parser_usecase.scenario).length === 0)
                                            delete parser_usecase.scenario;

                                        for (var i in parser_usecase.preconditions) {
                                            if (Object.keys(parser_usecase.preconditions[i]).length === 0)
                                                delete parser_usecase.preconditions[i];
                                        }
                                        parser_usecase.preconditions =
                                            parser_usecase.preconditions.filter(Object);
                                        if (parser_usecase.preconditions.length === 0)
                                            delete parser_usecase.preconditions;

                                        for (var i in parser_usecase.postconditions) {
                                            if (Object.keys(parser_usecase.postconditions[i]).length === 0)
                                                delete parser_usecase.postconditions[i];
                                        }
                                        parser_usecase.postconditions =
                                            parser_usecase.postconditions.filter(Object);
                                        if (parser_usecase.postconditions.length === 0)
                                            delete parser_usecase.postconditions;



                                        // add triggers steps that are not included in the db_steps
                                        for (var i in triggers_orig) {
                                            // check if step in db_steps
                                            var skip = false;
                                            for (var j in db_steps)
                                                if (db_steps[j].name === triggers_orig[i].name
                                                        && db_steps[j].step_type_id === 1) {
                                                    skip = true;
                                                    break;
                                                }
                                            if (skip) continue;


                                            var step = triggers_orig[i],
                                                arguments = [];

                                            for (var i in step.args) {
                                                arguments.push({
                                                    value: step.args[i]
                                                });
                                            };

                                            // add new
                                            steps.create.push({
                                                no:             step.no,
                                                name:           step.name,
                                                orig_name:           step.orig_name,
                                                played_by:      step.played_by,
                                                step_type_id:   1,
                                                argument:       arguments,
                                                project_id:     project_id,
                                                usecase:        usecase
                                            });

                                        }


                                        // add scenario steps that are not included in the db_steps
                                        for (var step_no in steps_orig) {
                                            // check if step in db_steps
                                            var skip = false;
                                            for (var j in db_steps)
                                                if (db_steps[j].no === step_no
                                                        && db_steps[j].name === steps_orig[step_no].name
                                                        && (db_steps[j].step_type_id === 2 || db_steps[j].step_type_id === 3)) {
                                                    skip = true;
                                                    break;
                                                }
                                            if (skip) continue;


                                            var step = steps_orig[step_no],
                                                arguments = [];

                                            for (var i in step.args) {
                                                arguments.push({
                                                    value: step.args[i]
                                                });
                                            };

                                            // add new
                                            steps.create.push({
                                                no:             step.no,
                                                name:           step.name,
                                                orig_name:           step.orig_name,
                                                played_by:      step.played_by,
                                                step_type_id:   (step.type === "step") ? 2 : 3,
                                                argument:       arguments,
                                                project_id:     project_id,
                                                usecase:        usecase
                                            });

                                        }


                                        // add preconditions steps that are not included in the db_steps
                                        for (var i in preconditions_orig) {
                                            // check if step in db_steps
                                            var skip = false;
                                            for (var j in db_steps)
                                                if (db_steps[j].name === preconditions_orig[i].name
                                                    && db_steps[j].step_type_id === 4) {
                                                    skip = true;
                                                    break;
                                                }
                                            if (skip) continue;


                                            var step = preconditions_orig[i],
                                                arguments = [];

                                            for (var i in step.args) {
                                                arguments.push({
                                                    value: step.args[i]
                                                });
                                            };

                                            // add new
                                            steps.create.push({
                                                no:             step.no,
                                                name:           step.name,
                                                orig_name:           step.orig_name,
                                                played_by:      step.played_by,
                                                step_type_id:   4,
                                                argument:       arguments,
                                                project_id:     project_id,
                                                usecase:        usecase
                                            });

                                        }


                                        // add postconditions steps that are not included in the db_steps
                                        for (var i in postconditions_orig) {
                                            // check if step in db_steps
                                            var skip = false;
                                            for (var j in db_steps)
                                                if (db_steps[j].name === postconditions_orig[i].name
                                                    && db_steps[j].step_type_id === 5) {
                                                    skip = true;
                                                    break;
                                                }
                                            if (skip) continue;


                                            var step = postconditions_orig[i],
                                                arguments = [];

                                            for (var i in step.args) {
                                                arguments.push({
                                                    value: step.args[i]
                                                });
                                            };

                                            // add new
                                            steps.create.push({
                                                no:             step.no,
                                                name:           step.name,
                                                orig_name:           step.orig_name,
                                                played_by:      step.played_by,
                                                step_type_id:   5,
                                                argument:       arguments,
                                                project_id:     project_id,
                                                usecase:        usecase
                                            });

                                        }


                                        /*
                                         * Do update!
                                         *
                                         * 1. delete actors from db
                                         */
                                        usecase.removeActor(actors.remove, function (err) {
                                            if (err) throw err;


                                            var actor_ids = [-1];
                                            for (var i in actors.remove) actor_ids.push(actors.remove[i].id);

                                            Actor.find({id: actor_ids}).remove(function (err) {
                                                if (err) throw err;

                                                /*
                                                 * 2. create new actors
                                                 */
                                                Actor.create(actors.create, function (err, new_actors) {
                                                    if (err) throw err;

                                                    /*
                                                     * 3. update existing actors
                                                     */
                                                    usecase.setActor(actors.update.concat(new_actors), function (err) {
                                                        if (err) throw err;

                                                        /*
                                                         * delete extension_points
                                                         */
                                                        usecase.removeExtensionPoint(extension_points.remove, function (err) {
                                                            if (err) throw err;


                                                            var extension_point_ids = [-1];
                                                            for (var i in extension_points.remove) extension_point_ids.push(extension_points.remove[i].id);

                                                            ExtensionPoint.find({id: extension_point_ids}).remove(function (err) {
                                                                if (err) throw err;

                                                                /*
                                                                 * create  extension_points
                                                                 */
                                                                ExtensionPoint.create(extension_points.create, function (err, new_extension_points) {
                                                                    // console.log(extension_points.create);
                                                                    if (err) throw err;

                                                                    /*
                                                                     * update existing  extension_points
                                                                     */
                                                                    usecase.setExtensionPoint(extension_points.update.concat(new_extension_points), function (err) {
                                                                        if (err) throw err;


                                                                        /*
                                                                         * 4. delete steps from db
                                                                         */
                                                                        scenario.removeStep(steps.remove, function (err) {
                                                                            if (err) throw err;


                                                                            var step_ids = [-1];
                                                                            for (var i in steps.remove) step_ids.push(steps.remove[i].id);


                                                                            Step.find({id: step_ids}).each(function (step) {
                                                                                step.argument = [];

                                                                            }).save(function (err) {
                                                                                if (err) throw err;

                                                                                Step.find({id: step_ids}).remove(function (err) {
                                                                                    if (err) throw err;


                                                                                    Argument.find({id: argument_remove_ids}).remove(function (err) {
                                                                                        if (err) throw err;



                                                                                        /*
                                                                                         * 5. create new steps
                                                                                         */
                                                                                        Step.create(steps.create, function (err, new_steps) {
                                                                                            if (err) throw err;

                                                                                            /*
                                                                                             * 6. update existing steps
                                                                                             */
                                                                                            scenario.setStep(steps.update.concat(new_steps), function (err) {
                                                                                                if (err) throw err;

                                                                                                    done(usecase, parser_usecase);


                                                                                            });

                                                                                        });

                                                                                    });
                                                                                });
                                                                            });
                                                                        });
                                                                    });
                                                                });
                                                            });
                                                        });


                                                    });
                                                });
                                            });
                                        });

                                    });


                                });
                            });


                        });
                    });
                });
            }

        });

    };


    Usecase.upsert_files = function (usecase, parser_files, done) {

        var Cls = db.models.cls;

        usecase.getFile(function (err, db_files) {
            if (err) throw err;


            var changes = {
                    remove:   [],
                    create:   [],
                    update:   []
                },
                File = db.models.file,
                cls_fragment_changes = {},
                cls_fragment_creates = [];

            /*
             * real updates: in case of DB, we need to store in update all items (to set realated table)
             * that's why there are also real_updates, they are passed to done()
             */
            var real_updates = [];

            // if files in database are not found in parser_files, delete them from db
            for (var i in db_files) {
                var db_file = db_files[i],
                    found = false;

                for (var j in parser_files) {
                    if (parser_files[j].path === db_file.path) {
                        found = true;

                        // by default update tags
                        db_file.tags = JSON.stringify(parser_files[j].tags);

                        // if they are found, check for changes
                        if (db_file.body != parser_files[j].body) {
                            db_file.body = parser_files[j].body;


                            if (parser_files[j].fragments.length > 0) {


                                if (db_file.cls_id !== undefined &&
                                        db_file.path === parser_files[j].path) {

                                    if (!(db_file.cls_id in cls_fragment_changes)) {
                                        cls_fragment_changes[db_file.cls_id] = [];
                                    }
                                    cls_fragment_changes[db_file.cls_id] =
                                        cls_fragment_changes[db_file.cls_id].concat(parser_files[j].fragments);


                                } else {

                                    var cls = new Cls();
                                    cls.name = parser_files[j].name;
                                    db_file.cls = cls;

                                    cls_fragment_creates.push({
                                        cls: cls,
                                        fragments: parser_files[j].fragments
                                    });
                                }

                                // this will not insert to related table :-( that's why there is cls_fragment_{updates,creates}
                                //cls.fragments = parser_files[j].fragments;

                            }
                            changes.update.push(db_file);
                            real_updates.push(db_file);
                        } else {
                            changes.update.push(db_file);
                        }
                        break;
                    }
                }

                if (!found) {
                    changes.remove.push(db_file);
                }
            }


            // if files in parser are not found in db, create them
            for (var i in parser_files) {
                var parser_file = parser_files[i],
                    found = false;

                for (var j in db_files) {
                    if (db_files[j].path === parser_file.path) {
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    var f = {
                        path: parser_file.path,
                        body: parser_file.body,
                        tags: JSON.stringify(parser_file.tags)
                    };
                    if (parser_file.fragments.length > 0) {
                        var cls = new Cls();
                        cls.name = parser_file.name;

                        // this will not insert to related table :-(
                        //cls.fragments = parser_file.fragments;
                        cls_fragment_creates.push({
                            cls: cls,
                            fragments: parser_file.fragments
                        });

                        f.cls = cls;
                    }
                    changes.create.push(f);
                }
            }


            /*
             * Do update!
             *
             * 1. delete files from db
             */
            usecase.removeFile(changes.remove, function (err) {
                if (err) throw err;

                var file_ids = [-1],
                    cls_ids = [0];
                for (var i in changes.remove) {
                    file_ids.push(changes.remove[i].id);
                    if (changes.remove[i].cls_id)
                        cls_ids.push(changes.remove[i].cls_id);
                }


                Cls.find({id: cls_ids}).each(function (cls) {
                    cls.fragment = [];

                }).save(function (err) {
                    if (err) throw err;

                    Cls.find({id: cls_ids}).remove(function (err) {
                        if (err) throw err;

                        File.find({id: file_ids}).remove(function (err) {
                            if (err) throw err;

                            /*
                             * 2. create new files
                             */
                            File.create(changes.create, function (err, new_files) {
                                if (err) throw err;

                                /*
                                 * 3. update existing files
                                 */
                                usecase.setFile(changes.update.concat(new_files), function (err) {
                                    if (err) throw err;


                                    /*
                                     * 4. set fragments to Files.classes
                                     */
                                    Cls.set_fragments(usecase, cls_fragment_creates, cls_fragment_changes, function () {

                                        done(usecase, {
                                            remove:   changes.remove,
                                            create:   changes.create,
                                            update:   real_updates
                                        });
                                    });
                                });

                            });
                        });
                    });
                });
            });



        });
    };

    Usecase.fetch_all = function (done) {

        Usecase.find({ }, function (err, usecases) {
            if (err) throw err;

            var usecases_o = [];

            for (var i in usecases) {
                usecases_o.push(JSON.parse(usecases[i].parser));
            }

            done(usecases_o);
        });

    };

    return Usecase;
};

