
module.exports = function (orm, db) {
    var File = db.define('file', {
            path            : { type: 'text', required: true },
            tags            : { type: 'text', required: true },
            body            : { type: 'text', required: true },
            created_at      : { type: 'date', required: true, time: true }
        },
        {
            hooks: {
                beforeValidation: function () {
                    this.created_at = new Date();
                }

            },
            methods: {

            }
        });

    File.hasOne('cls', db.models.cls, {}, { key: true, autoFetch : true });

    return File;
};