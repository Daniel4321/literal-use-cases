var orm      = require('orm'),
    transaction = require('orm-transaction'),
    cfg      = require('_/config');

var connection = null;

function setup(db, cb) {

    //require('./template_file.js')(orm, db);
    //require('./cls.js')(orm, db);
    //require('./argument.js')(orm, db);
    //require('./actor.js')(orm, db);
    //require('./step_type.js')(orm, db);
    //require('./step.js')(orm, db);
    //require('./scenario.js')(orm, db);
    //require('./file.js')(orm, db);
    //require('./usecase.js')(orm, db);
    //require('./fragment.js')(orm, db);


    require('./argument.js')(orm, db);
    require('./step_type.js')(orm, db);
    var Step = require('./step.js')(orm, db);
    require('./scenario.js')(orm, db);
    require('./actor.js')(orm, db);

    var Project = require('./project.js')(orm, db);
    // requires file, but not yet loaded
    var Usecase = require('./usecase.js')(orm, db);
    require('./fragment.js')(orm, db);
    require('./cls.js')(orm, db);
    var File = require('./file.js')(orm, db);
    require('./template_file.js')(orm, db);


    Step.hasOne('usecase', Usecase, {}, { key: true, autoFetch : true });
    Step.hasOne('project', Project, {}, { key: true, autoFetch : true });

    // relationships
    require('./relationship.js')(orm, db);
    var ExtensionPoint = require('./extension_point.js')(orm, db);
    Usecase.hasMany('extension_point', db.models.extension_point, {}, { cascadeRemove: true, key: true });

    var UsecaseFile = db.define('usecase_file', {
            usecase_id : { type: 'integer', key: true },
            file_id  : { type: 'integer', key: true }
        },
        {
            hooks: {
            },
            methods: {
            }
        });
    UsecaseFile.hasOne('usecase', Usecase, {}, { key: true, autoFetch : true });
    UsecaseFile.hasOne('file', File, {}, { key: true, autoFetch : true });

    Usecase.hasMany('file', File, {
        //id:      {type: 'serial', key: true}
    }, { cascadeRemove: true, key: true, autoFetch : true });

    return cb(null, db);
}

module.exports = function (cb) {
    if (connection) return cb(null, connection);

    orm.connect(cfg.database, function (err, db) {
        if (err) return cb(err);

        db.use(transaction);

        connection = db;

        db.settings.set('instance.cache', false);
        db.settings.set('instance.returnAllErrors', true);
        setup(db, cb);
    });
};