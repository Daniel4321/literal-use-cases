
var log = console.log.bind(console),
    fs = require('fs'),
    path = require('path'),
    cfg = require('_/config'),
    fstools = require('_/app/generator/fstools'),
    opened_projects = [];

module.exports = function (app, db) {

    this.app        = app;
    this.db         = db;
    this.models     = db.models;

    this.new = function (project_data, done) {

        var _this = this,
            Project =  this.models.project,
            project = new Project();

        project.name = project_data.name;
        project.user = project_data.user;
        project.project_path = project_data.project_path;
        project.migration_path = project_data.migration_path;
        project.tpl_path = project_data.tpl_path;
        project.gen_path = project_data.gen_path;

        project.save(function (err) {
            if (err) throw err;

            done({project_id:project.id});
        });
    };
    this.getAll = function (user, done) {
        var _this = this,
            Project =  this.models.project;

        Project.find({user: user}, function (err, projects) {
            if (err) throw err;
            done(projects);
        });
    };
    this.open = function (project_id, done) {

        var _this = this,
            Application = this.app,
            Project =  this.models.project;

        Project.find({id: project_id}).each(function (project) {

            if (project.id in opened_projects) {
                done(project);
                return;
            }

            var cfg_clone = JSON.parse(JSON.stringify(cfg));
            var cfg_custom = {
                project_id: project_id,
                project: project,
                migration_path: project.migration_path,
                tpl_path: project.tpl_path,
                usecase_path: project.tpl_path,
                gen_path: project.gen_path
            };
            for (var attrname in cfg_custom) { cfg_clone[attrname] = cfg_custom[attrname]; }
            var app = new Application(cfg_clone, _this.db);
            opened_projects[project.id] = app;

            fstools.mkdirpAsync(project.project_path, function () {
                fstools.mkdirpAsync(project.migration_path, function () {
                    fstools.mkdirpAsync(project.tpl_path, function () {
                        fstools.mkdirpAsync(project.gen_path, function () {

                            app.watch();
                            done(project);

                        });
                    });
                });
            });


        });


    };
    this.registerObserver = function (project_id, observer, done) {

        if (!(project_id in opened_projects)) {
            done({error: 'Project is not opened'});
            return;
        } else {
            console.log('registering observer ', project_id);
            var app = opened_projects[project_id];
            app.logger.registerObserver(observer);
            done();
        }

    };
};