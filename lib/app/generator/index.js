
var log = console.log.bind(console),
    fs = require('fs'),
    fstools = require('./fstools'),
    path = require('path'),
    jsdiff = require('diff'),
    tpl = require('_/micro-templating'),
    Promise = require('promise'),
    utils   = require('_/parser/utils.js'),
    parser   = require('_/parser'),
    helper = {
        diffExtractChanges: function (diffs) {
            var changes = [];
            for (var i in diffs) {
                if (diffs[i].added || diffs[i].removed) {
                    changes.push(diffs[i])
                }
            }
            return changes
        }
    };


module.exports = function (cfg) {

    this.cfg = cfg;

    this.syncFilesForUsecase = function (settings, done) {

        var _this = this,
            usecase = settings.usecase,
            merged_files = settings.merged_files,
            usecase_files_before_change = settings.usecase_files_before_change,
            changes = settings.changes,
            diffs = [],

            Diff = function (path, diff) {
                this.path = path;
                this.diff = diff;
            };

        if (usecase.name !== null &&
                usecase.level !== null) {

            var replacements = {
                    "%gen_path%":   this.cfg.gen_path,
                    "%level%":      usecase.level,
                    "%name%":       utils.camelCase(usecase.name)
                },
                usecase_path = this.cfg.gen_usecase_path,
                model_path = this.cfg.gen_model_path;

            usecase_path = usecase_path.replace(/%\w+%/g, function(all) {
                return replacements[all] || all;
            });

            model_path = model_path.replace(/%\w+%/g, function(all) {
                return replacements[all] || all;
            });


            /*
             * Create/update files
             */
            var files = changes.create.concat(changes.update),
                create = [],
                remove = [];


            function do_remove(path) {
                return new Promise(function (fulfill, reject) {
                    fs.exists(path, function (exists) {
                        if (exists) {
                            fs.unlink(path, function () {
                                fulfill()
                            });
                        } else {
                            fulfill()
                        }
                    });
                });
            }

            //log(files);
            for (var i in files) {
                var file        = files[i],
                    path        = null,
                    body        = tpl.parseTemplate(file.body, {
                        usecase: usecase,
                        utils: utils
                    });

                //log(file.body);
                //log (file);


                // find cls_id in cls_fragment_mappings to obtain fragments
                var fragments = [];
                for (var j in merged_files) {
                    if (merged_files[j].path === file.path) {
                        fragments = fragments.concat(merged_files[j].fragments);
                    }
                }

                if (file.cls_id !== null && fragments.length > 1) {
                    path = model_path+'/'+file.path;

                    /**
                     * Go through fragments and delete old files, if exists
                     */
                    for (var j in fragments) {
                        var fragment = fragments[j],
                            replace = {
                                "%gen_path%":   this.cfg.gen_path,
                                "%level%":      fragment.usecase.level,
                                "%name%":       utils.camelCase(fragment.usecase.name)
                            };

                        usecase_path = this.cfg.gen_usecase_path.replace(/%\w+%/g, function(all) {
                            return replace[all] || all;
                        });
                        var old_path = usecase_path+'/'+file.path;


                        remove.push(do_remove(old_path));
                    }
                } else {
                    path = usecase_path+'/'+file.path;
                }


                function do_create(path, file, body) {
                    return new Promise(function (fulfill, reject) {
                        fstools.mkdirpAsync(path, function () {

                            fs.exists(path, function (exists) {
                                if (exists) {

                                    fs.readFile(path, {encoding: "UTF-8"}, function (err, data) {
                                        if (err) throw err;

                                        var cmp = null;
                                        for (var i in usecase_files_before_change) {
                                            if (file.path === usecase_files_before_change[i].path) {
                                                cmp = usecase_files_before_change[i].body;
                                            }
                                        }

                                        var ch = helper.diffExtractChanges(
                                            jsdiff.diffLines(cmp ? cmp : data, file.body));

                                        if (ch.length > 0)
                                            diffs.push(new Diff(
                                                file.path,
                                                ch
                                            ));

                                        fs.writeFile(path, body, function (err) {
                                            if (err) throw err;
                                            fulfill();
                                        })

                                    });


                                } else {

                                    var ch = helper.diffExtractChanges(jsdiff.diffLines("", file.body));
                                    if (ch.length > 0)
                                        diffs.push(new Diff(
                                            file.path,
                                            ch
                                        ));

                                    fs.writeFile(path, body, function (err) {
                                        if (err) throw err;
                                        fulfill();
                                    });

                                }
                            })
                        });

                    });
                }

                create.push(do_create(path, file, body));
            }


            /*
             * Delete files
             */
            files = changes.remove;

            for (var i in files) {
                var file = files[i],
                    path = usecase_path + '/' + file.path;

                remove.push(do_remove(path));
            }


            Promise.all(remove.concat(create)).then(function () {
                /*
                 * Recursive remove empty directories
                 */
                fstools.removeEmptyFoldersRecursiveAsync(_this.cfg.gen_path, function () {

                    done(diffs);
                });

            });


        }

    };


    this.generateTemplate = function (template_file, usecases, done) {

        var _this = this,
            path = this.cfg.gen_path+template_file.replace(this.cfg.tpl_path, '');
        //console.log(this.cfg.tpl_path);
        //console.log(template_file);
        //console.log(path);

        fstools.fileIsAscii(template_file, function (isAscii) {

            // binary files just copy 
            if (!isAscii) {
                fstools.copy(template_file, path, function (err) {
                    if (err) throw err;
                    done([]);
                });
                return;
            }

            for (var i in _this.cfg.tpl_ignore_paths) {
                if (path.substring(0, _this.cfg.tpl_ignore_paths[i].length) === _this.cfg.tpl_ignore_paths[i]) {
                    //log ('ignore');
                    done([]);
                    return;
                }
            }

            for (var i in _this.cfg.tpl_copy_paths) {
                if (path.substring(0, _this.cfg.tpl_copy_paths[i].length) === _this.cfg.tpl_copy_paths[i]) {
                    //log ('copy');
                    fstools.copy(template_file, path, function (err) {
                        if (err) throw err;
                        done([]);
                    });
                    return;
                }
            }


            if (new RegExp("."+_this.cfg.file_extension+"$").test(path)) {
                path = path.replace(new RegExp("."+_this.cfg.file_extension+"$"), '');
            }

            fstools.mkdirpAsync(path, function () {

                fs.readFile(template_file, {encoding: "UTF-8"}, function (err, data) {
                    if (err) throw err;

                    /**
                     * Prepare paths to app
                     */
                    for (var i in usecases) {
                        usecases[i].path = usecases[i].path.replace(
                            new RegExp("^"+_this.cfg.tpl_path), ''
                        );

                        var replace = {
                                "%gen_path%":   '%gen_path%',
                                "%level%":      usecases[i].level,
                                "%name%":       utils.camelCase(usecases[i].name)
                        },
                            pathDir = _this.cfg.gen_usecase_path.replace(/%\w+%/g, function(all) {
                                return replace[all] || all;
                            }).replace('%gen_path%', '');

                        usecases[i].pathDir = pathDir;
                    }

                    var generated = tpl.parseTemplate(data, {
                        usecases: usecases,
                        utils: utils
                    });

                    fs.exists(path, function (exists) {
                        if (exists) {
                            fs.readFile(path, {encoding: "UTF-8"}, function (err, data) {

                                fs.writeFile(path, generated, function (err) {
                                    if (err) throw err;

                                    var change = helper.diffExtractChanges(
                                        jsdiff.diffLines(data, generated)
                                    );

                                    if (change.length === 0) {
                                        done([]);
                                    } else {
                                        done([{
                                            path: path,
                                            diff: change
                                        }]);
                                    }
                                });
                            });
                        } else {

                            fs.writeFile(path, generated, function (err) {
                                if (err) throw err;

                                done([]);
                            });
                        }
                    });


                });

            });
        });



    };

    this.removeTemplate = function (template_file, done) {


        var path = this.cfg.gen_path+template_file.replace(this.cfg.tpl_path, ''),
            _this = this;


        for (var i in this.cfg.tpl_ignore_paths) {
            if (path.substring(0, this.cfg.tpl_ignore_paths[i].length) === this.cfg.tpl_ignore_paths[i]) {
                //log ('ignore');
                done();
                return;
            }
        }


        if (new RegExp("."+this.cfg.file_extension+"$").test(path)) {
            path = path.replace(new RegExp("."+this.cfg.file_extension+"$"), '');
        }

        fs.exists(path, function (exists) {
            if (exists) {
                fs.unlink(path, function () {
                    fstools.removeEmptyFoldersRecursiveAsync(_this.cfg.gen_path, function () {
                        done();
                    });
                });
            } else {
                done();
            }
        });

    };

    this.removeUsecase = function (usecase, done) {

        var replacements = {
                "%gen_path%":   this.cfg.gen_path,
                "%level%":      usecase.level,
                "%name%":       utils.camelCase(usecase.name)
            },

            usecase_path = this.cfg.gen_usecase_path.replace(/%\w+%/g, function(all) {
                return replacements[all] || all;
            });


        fstools.removeFolderRecursiveAsync(usecase_path, function () {
            done();
        });

    };

};