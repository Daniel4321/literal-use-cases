
var fs = require('fs'),
    path = require('path'),
    Promise = require('promise');

module.exports = {


    mkdirpAsync: function (path, done) {

        var path_split  = path.split('/'),
            path_partial = "",
            create = [];

        /*
         * Ensure path is created
         */
        for (var i in path_split) {

            // last is filename
            if (path_split.length-1 <= i)
                break;

            path_partial += path_split[i]+'/';
            if (path_split[i] === "..") continue;


            function do_create (path_partial) {
                return function () {
                    return new Promise(function (fulfill, reject) {
                        fs.exists(path_partial, function (exists) {
                            if (!exists) {
                                fs.mkdir(path_partial, function (err) {
                                    //if (err) throw err;
                                    fulfill();
                                })
                            } else {
                                fulfill();
                            }
                        });
                    });
                }
            }

            create.push(do_create(path_partial));
        }

        create.reduce(function(cur, next) {
            return cur.then(next);
        }, Promise.resolve()).then(function () {
            done();
        });
    },

    removeEmptyFoldersRecursiveAsync: function (dir, done) {
        var _this = this;

        fs.exists(dir, function (exists) {
            if (exists) {

                fs.readdir(dir, function (err, list) {

                    var remove = [];

                    for (var i in list) {

                        function do_remove(fn) {

                            return new Promise(function (fulfill, reject) {
                                var filename = path.join(dir, fn);

                                fs.stat(filename, function (err, stat) {

                                    if (filename in [".", ".."]) {
                                        // skip
                                        fulfill();
                                    } else if (stat.isDirectory()) {

                                        // recurse
                                        _this.removeEmptyFoldersRecursiveAsync(filename, function () {
                                            fulfill();
                                        });

                                    } else {
                                        // it is file, leave it
                                        fulfill();
                                    }
                                });
                            });
                        }

                        remove.push(do_remove(list[i]));
                    }

                    Promise.all(remove).then(function () {

                        fs.readdir(dir, function (err, list) {
                            if (list.length === 0) {
                                fs.rmdir(dir, function () {
                                    done();
                                });
                            } else {
                                done();
                            }
                        });
                    });

                });

            }
        });

    },

    /**
     * @author Yoav Niran
     *
     * https://gist.github.com/yoavniran/adbbe12ddf7978e070c0
     */
    removeFolderRecursiveAsync: function (dirToRemove, callback) {

        var dirList = [];
        var fileList = [];

        function flattenDeleteLists(fsPath, callback) {

            fs.lstat(fsPath, function (err, stats) {

                if (err) {
                    callback(err);
                    return;
                }

                if (stats.isDirectory()) {

                    dirList.unshift(fsPath);  //add to our list of dirs to delete after we're done exploring for files

                    fs.readdir(fsPath, function (err, files) {

                        if (err) {
                            callback(err);
                            return;
                        }

                        var currentTotal = files.length;

                        var checkCounter = function (err) {
                            if (currentTotal < 1 || err) {
                                callback(err);
                            }
                        };

                        if (files.length > 0) {
                            files.forEach(function (f) {
                                flattenDeleteLists(path.join(fsPath, f), function (err) {
                                    currentTotal -= 1;
                                    checkCounter(err);
                                });
                            });
                        }

                        checkCounter(); //make sure we bubble the callbacks all the way out
                    });
                }
                else {
                    fileList.unshift(fsPath); //add to our list of files to delete after we're done exploring for files
                    callback();
                }
            });
        }

        function removeItemsList(list, rmMethod, callback) {

            var count = list.length;

            if (count === 0){
                callback();
                return;
            }

            list.forEach(function (file) {
                fs[rmMethod](file, function (err) {
                    count -= 1;
                    if (count < 1 || err) {
                        callback(err);
                    }
                });
            });
        }

        function onFinishedFlattening(err) {

            if (err) {
                callback(err);
                return;
            }

            removeItemsList(fileList, "unlink", function (err) {//done exploring folders without errors
                if (err) {
                    callback(err);
                    return;
                }

                removeItemsList(dirList, "rmdir", function (err) { //done deleting files without errors
                    callback(err);  //done
                });
            });
        }

        flattenDeleteLists(dirToRemove, onFinishedFlattening);
    },


    copy: function (source, target, cb) {
        var cbCalled = false;

        var rd = fs.createReadStream(source);
        rd.on("error", function(err) {
            done(err);
        });
        var wr = fs.createWriteStream(target);
        wr.on("error", function(err) {
            done(err);
        });
        wr.on("close", function(ex) {
            done();
        });
        rd.pipe(wr);

        function done(err) {
            if (!cbCalled) {
                cb(err);
                cbCalled = true;
            }
        }
    },

    fileIsAscii: function (filename, callback) {
        // Read the file with no encoding for raw buffer access.
        fs.readFile(filename, function(err, buf) {
            if (err) throw err;
            var isAscii = true;
            for (var i=0, len=buf.length; i<len; i++) {
                if (buf[i] > 127) { isAscii=false; break; }
            }
            callback(isAscii); // true iff all octets are in [0, 127].
        });
    }

};