
var chokidar = require('chokidar'),
    log = console.log.bind(console),
    parser = require('_/parser'),
    Logger = require('_/logger'),
    Generator = require('_/app/generator'),
    Sync = require('_/app/sync'),
    Merger = require('_/app/merger'),
    dq = require ("deferred-queue"),
    Promise = require('promise'),
    queue = dq();

module.exports = function (cfg, db) {

    this.logger     = new Logger(['html']);
    this.db         = db;
    this.models     = db.models;
    this.cfg        = cfg;

    this.watch = function () {
        var _this = this;
        this.watchUsecases(this.cfg.usecase_path, function () {
            _this.watchTemplate(_this.cfg.tpl_path);
        });
    };

    this.watchUsecases = function (folder, afterInit) {

        var app             = this,
            src_path        = folder,
            watcher         = chokidar.watch(src_path),
            fixPath = function (path) {return './'+path;};


        watcher
            .on('add', function(path) {
                // relative
                path=fixPath(path);

                queue.push(function (cb) {

                    app.logger.logFileChange(path, 'added');
                    app.updateUsecase(path, cb)
                });
            })
            .on('change', function(path) {
                // relative
                path=fixPath(path);

                queue.push(function (cb) {

                    app.logger.logFileChange(path, 'changed');
                    app.updateUsecase(path, cb)
                });
            })
            .on('unlink', function(path) {
                // relative
                path=fixPath(path);

                queue.push(function (cb) {
                    app.logger.logFileChange(path, 'removed');
                    app.removeUsecase(path, cb);
                });
            })
            .on('ready', function() {

                queue.push(function (cb) {
                    afterInit();
                    cb();
                });
            });

    };


    this.watchTemplate = function (folder) {

        var app             = this,
            watcher         = chokidar.watch(folder),
            generator       = new Generator(this.cfg),
            models = app.models,
            Usecase = models.usecase,
            fixPath = function (path) {return './'+path;};

        watcher
            .on('add', function(path) {
                queue.push(function (cb) {
                    // relative
                    path=fixPath(path);

                    app.logger.logFileChange(path, 'added (template)');
                    Usecase.fetch_all(function (usecases) {
                        generator.generateTemplate(path, usecases, function (diffs) {

                            // print file diffs
                            if (diffs.length !== 0) {
                                //log(JSON.stringify(diffs, null, 2));
                                app.logger.logFileDiffs(diffs);
                            }
                            cb();
                        });

                    });
                });
            })
            .on('change', function(path) {
                queue.push(function (cb) {

                    // relative
                    path=fixPath(path);

                    app.logger.logFileChange(path, 'changed (template)');
                    Usecase.fetch_all(function (usecases) {
                        generator.generateTemplate(path, usecases, function (diffs) {

                            // print file diffs
                            if (diffs.length !== 0) {
                                app.logger.logFileDiffs(diffs);
                            }
                            cb();
                        });
                    });
                });
            })
            .on('unlink', function(path) {

                // relative
                path=fixPath(path);

                queue.push(function (cb) {
                    app.logger.logFileChange(path, 'removed (template)');
                    generator.removeTemplate(path, function () {
                        cb();
                    });
                });
            })
            .on('ready', function() {

                queue.push(function (cb) {
                    cb();
                });
            });

    };

    /**
     * Process of updating use case
     * ----------------------------
     *
     * 1. Parse text-form of use case
     * 2. Update model according to parsed use case
     * 3. Parse files inside use case
     * 4. Merge classes with the same name
     * 5. Update model according to merged files
     * 6. Synchronize file system
     *
     * See enclosed diagram model/model.puml
     *
     * @param path
     * @param done
     */
    this.updateUsecase = function (path, done) {

        var app = this,//require('_/app'),
            _this = this,
            models = app.models,
            db = app.db,
            Usecase = models.usecase,
            parserObj = new parser.Usecase(),
            generator = new Generator(this.cfg),
            sync = new Sync({ gen_path: this.cfg.gen_path, models: models }),
            merger = new Merger(models),

            /**
             * 1.
             *
             * Parser is a standalone module, it just parses path and returns object
             */
            parser_usecase = parserObj.parseUsecase(path);

        db.transaction(function (err, transaction) {

            /**
             * 2.
             */
            Usecase.upsert(parser_usecase, _this.cfg.project_id, function (usecase, changes) {
                if (err) throw err;

                if (usecase === null) {
                    transaction.rollback(function (err) {
                        if (err) throw err;

                        done();
                        // Done!
                    });
                    return;
                }

                // delete path in log
                delete changes.path;

                // print changes
                if (Object.keys(changes).length !== 0) {
                    app.logger.logUsecaseChanges(usecase, 0, changes);
                }

                // save files before change to preserve information
                var usecase_files_before_change = [];
                for (var i in usecase.file) {
                    usecase_files_before_change.push({
                        path: usecase.file[i].path,
                        body: usecase.file[i].body
                    });
                }



                /**
                 * 3. Files are actually fragments
                 */
                var files = parserObj.parseFiles(path);

                /**
                 * 4.
                 *
                 * If there is an sync tag, find all the same fragments and update them in DB and FS.
                 * Fragments are the same, if they all have the same filepath and @sync tag.
                 */
                sync.syncFiles(files, function () {


                    /**
                     * 5.
                     *
                     * Actually, this will transform files to fragments
                     *
                     * Pay ATTENTION, this will:
                     * 5.1. Delete changed fragments of this concrete use case
                     * 5.2. Create new Fragment objects in database according to
                     *      - deleted changed fragments
                     *      - new fragments in files
                     * 5.3. Merge fragments and return as File objects with additional array of fragments
                     */
                    merger.mergeSameClasses(usecase, files, function (merged_files) {

                        /**
                         * 6.
                         *
                         * Files and the merged files are updated in database
                         */
                        Usecase.upsert_files(usecase, merged_files, function (usecase, changes) {

                            /**
                             * 7.
                             *
                             * All files are synchronized with the filesystem
                             */
                            generator.syncFilesForUsecase({
                                usecase: usecase,
                                usecase_files_before_change: usecase_files_before_change,
                                merged_files: merged_files,
                                changes: changes
                            }, function (diffs) {

                                // print file diffs
                                if (diffs.length !== 0) {
                                    //log(JSON.stringify(diffs, null, 2));
                                    app.logger.logFileDiffs(diffs);
                                }

                                transaction.commit(function (err) {
                                    if (err) throw err;

                                    done();
                                    // Done!
                                });
                            });
                        });

                    });

                });
            });

        });



    };


    this.removeUsecase = function (path, done) {

        var app = this,//require('_/app'),
            models = app.models,
            db = app.db,
            generator = new Generator(this.cfg),
            Usecase = models.usecase;

        db.transaction(function (err, transaction) {
            if (err) throw err;

            Usecase.find({path: path}, function (err, usecase) {
                if (err) throw err;
                usecase = usecase[0];

                /**
                 * Remove from database
                 */
                usecase.cascadeRemove(function () {


                    transaction.commit(function (err) {
                        if (err) throw err;


                        generator.removeUsecase(usecase, function () {

                            done();
                            // Done!
                        });

                    });
                });


            });

        });


    };

};