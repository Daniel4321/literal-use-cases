
var log = console.log.bind(console),
    fs = require('fs'),
    path = require('path'),
    jsdiff = require('diff'),
    tpl = require('_/micro-templating'),
    utils   = require('_/parser/utils.js'),
    parser   = require('_/parser'),
    Promise = require('promise'),
    app             = require('_/app'),
    orm = require("orm");

module.exports = function (models) {

    this.models = models;

    /**
     * return files with fragments attribute
     */
    this.mergeSameClasses = function (usecase, files, done) {

        var _this = this,
            merged_files = [],
            updates = [];

        this.removeFragmentsPerUsecase(usecase, files)

            .done(function (map_preserve_db_file) {


                for (var i in files) {
                    var file = files[i],
                        path = file.path,
                        lang = path.substr(path.lastIndexOf(".") + 1),
                        classes_tree = parser.code.analyse(file.body, lang),
                        reuse_fragment_db = i in map_preserve_db_file ? map_preserve_db_file[i] : null;

                    if (classes_tree.length === 0) {
                        /**
                         * Nothing to do
                         */
                        files[i].fragments = [];
                        merged_files.push(files[i]);

                    } else {

                        /**
                         * Consider and merge only top-ones in the tree
                         */
                        var before_cls = [],
                            cls = null,
                            after_cls = [];
                        for (var j in classes_tree) {
                            if (cls !== null && classes_tree[j].type !== 'cls') {
                                after_cls.push(classes_tree[j]);
                            }
                            if (cls === null && classes_tree[j].type !== 'cls') {
                                before_cls.push(classes_tree[j]);
                            }
                            if (classes_tree[j].type === 'cls') {
                                cls = classes_tree[j];
                            }
                        }
                        updates.push(_this.merge(usecase, file.path, file.tags, cls, reuse_fragment_db, before_cls, after_cls));
                    }
                }

                /**
                 * ONE AFTER EACH OTHER, CHAIN RESULTS
                 */
                updates.reduce(function(cur, next) {
                    return cur.then(next);
                }, Promise.resolve()).then(function (result_array) {

                    if (result_array === null) {
                        result_array = [];
                    }

                    // get only last, cause they contains all inserted fragments
                    var key_val = {};
                    var key_val_langs = {};
                    for (var i in result_array) {
                        key_val[result_array[i].name] = result_array[i];
                        path = result_array[i].path;
                        key_val_langs[result_array[i].name] = path.substr(path.lastIndexOf(".") + 1);
                    }

                    /**
                     * FORMAT
                     */
                    for (var i in key_val) {
                        key_val[i].body = parser.formatter.format(key_val_langs[i], key_val[i].body);
                    }

                    for (var i in key_val) {
                        merged_files.push(key_val[i]);
                    }


                    done(merged_files);
                });
            });

    };

    this.removeFragmentsPerUsecase = function (usecase, files) {
        var _this = this,
            models = this.models;

            return new Promise(function (fulfill, reject) {

                models.fragment.find({usecase_id: usecase.id}, function (err, db_fragments) {

                    if (err)
                        throw err;

                    // check to delete, find files that didn't changed
                    var preserve = [], map_preserve_db_file = {};
                    for (var i in db_fragments) {
                        var fragment_from_db = db_fragments[i];

                        for (var j in files) {
                            var fragment_from_file = files[j];



                            if (fragment_from_db.file_body  === fragment_from_file.body) {
                                preserve.push(fragment_from_db.id);
                                map_preserve_db_file[j] = fragment_from_db;
                            }

                        }
                    }

                    //models.fragment.find({id: orm.not_in(preserve), usecase_id: usecase.id}, function (err, framents_to_remove) {
                    //
                    //    if (err)
                    //        throw err;
                    //
                    //    usecase.getFile(function (err, files) {
                    //        if (err)
                    //            throw err;
                    //
                    //
                    //
                    //    });
                    //
                    //    log(framents_to_remove);


                        models.fragment.find({id: orm.not_in(preserve), usecase_id: usecase.id}).remove(function (err) {
                            if (err)
                                throw err;

                            fulfill(map_preserve_db_file);

                        });
                    //});

                });


            });


    };

    this.merge = function (usecase,
                           filename,
                           tags,
                           cls,
                           reuse_fragment_db,
                           before_cls,
                           after_cls) {

        var _this = this,
            models = this.models,
            Fragment = models.fragment;

        return function (result_array) {


            if (result_array === undefined || result_array === null) {
                result_array = [];
            }

            return new Promise(function (fulfill, reject) {

                var fragment = null;
                if (reuse_fragment_db === null) {
                    fragment = new Fragment();
                } else {
                    fragment = reuse_fragment_db;
                }

                fragment.type = cls.type;
                fragment.name = cls.name;
                fragment.lang = cls.lang;
                fragment.signature = cls.signature;
                fragment.before_body = JSON.stringify(before_cls);
                fragment.body = cls.body;
                fragment.after_body  = JSON.stringify(after_cls);
                fragment.file_body = cls.file_body;

                fragment.usecase = usecase;
                fragment.usecase_id = usecase.id;
                fragment.parser = JSON.stringify(cls);

                fragment.save(function (err) {
                    if (err)
                        throw err;

                    Fragment.find({ name: cls.name, type: cls.type }, function (err, fragments) {
                        if (err)
                            throw err;


                        result_array.push(
                            _this.do_merge(filename, fragments, tags)
                        );

                        fulfill(result_array);

                    });

                });


            });
        };
    };

    /**
     * Do the actual merge of fragments
     */
    this.do_merge = function (filename, fragments, tags) {

        var before_body,
            body,
            after_body,
            Fragment = this.models.fragment;

        // at least one is there

        before_body = Fragment.constructOutsideBody('before_body', fragments);
        body        = Fragment.constructClsWithFragments(fragments);
        after_body  = Fragment.constructOutsideBody('after_body', fragments);

        return {
            name: fragments[0].name,
            path: filename,
            fragments: fragments,
            body: before_body+body+after_body,
            tags: tags
        }
    };
};