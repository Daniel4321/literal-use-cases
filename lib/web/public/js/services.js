
angular.module('app.services', [])

    .factory('FetchFileFactory', ['$http',
        function($http) {
            var _factory = {};

            _factory.fetchFile = function(file) {
                return $http.get('/api/resource?resource=' + encodeURIComponent(file));
            };
            _factory.saveFile = function(file) {
                return $http.post('/api/file/save', file);
            };
            _factory.removeFile = function(file) {
                return $http.post('/api/file/rm', file);
            };
            _factory.createFile = function(file) {
                return $http.post('/api/file/create', file);
            };

            return _factory;
        }
    ])
    .factory('Inspector', ['$http',
        function($http) {
            var _inspector = {};

            _inspector.inspect = function(file) {
                return $http.get('/api/inspect?resource=' + encodeURIComponent(file));
            };

            return _inspector;
        }
    ])
    .factory('Project', ['$http',
        function($http) {
            var _project = {};

            _project.new = function(project) {
                return $http.post('/api/project/new', project);
            };
            _project.open = function(project_id) {
                return $http.post('/api/project/open', {project_id: project_id});
            };
            _project.getAll = function(project_id) {
                return $http.get('/api/project/get_all');
            };

            return _project;
        }
    ])
    .factory('Usecase', ['$http',
        function($http) {
            var _usecase = {};

            _usecase.getAll = function(project_id) {
                return $http.post('/api/usecase/get_all', {project_id: project_id});
            };


            _usecase.get3D = function(project_id, usecases_selected) {
                return $http.post('/api/usecase/get_3d', {project_id: project_id, usecases_selected: usecases_selected});
            };

            return _usecase;
        }
    ])

    .factory('Console', ['$websocket', '$timeout',
        function($websocket, $timeout) {
            var dataStream = null;

            var collection = [];
            function nl2br(str) {
                return (str+'').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br/>' + '$2');
            }
            function space2nbsp(str) {
                // ^ is reserved for space :-)
                return str.replace(/\s/g, '&nbsp;').replace(/\^/g, ' ');
            }

            function connect(project_id) {
                console.log('connecting to websocket');
                dataStream = $websocket('ws://'+location.hostname+(location.port ? ':'+location.port: '')+'/data');

                dataStream.onOpen(function(){
                    // collection.push("Websockets connected...<br>");
                });
                dataStream.onMessage(function(message) {
                    var data = JSON.parse(message.data);
                    if (data.ready) {
                        // collection.push("Opening project...<br>");
                        dataStream.send(JSON.stringify({project_id: project_id}));
                    }
                    collection.push(space2nbsp(nl2br(data.message)));
                });
            }
            function disconnect() {
                console.log('disconnecting from websockets');
                if (dataStream)
                    dataStream.close();
                dataStream = null;
            }

            var methods = {
                collection: collection,
                connect: connect,
                disconnect: disconnect,
                get: function() {
                    if (dataStream)
                        dataStream.send(JSON.stringify({ action: 'get' }));
                }
            };

            return methods;
        }
    ])

;