
angular.module('app', ['app.controllers', 'app.services', 'ui.router', 'jsTree.directive', 'ngSanitize', 'ngWebSocket'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "tpl/menu.html",
                controller: 'AppCtrl'
            })

        ;

        $urlRouterProvider.otherwise('/app');
    });

