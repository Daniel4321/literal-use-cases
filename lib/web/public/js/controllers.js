
angular.module('app.controllers', [])

    .controller('MenuCtrl', ['$scope', '$timeout', 'Project', 'Console', 'Usecase', function ($scope, $timeout, Project, Console, Usecase) {

        $scope.exTwoView = function (refresh) {
            $scope.$emit('twoView', refresh);
            $scope.$emit('hideConsole', {});
        };


        $scope.saveFileClicked = function () {
            $scope.$emit('saveFileClicked', {});
        };


        $scope.newProject = function () {

            $scope.modalSource = 'tpl/new-project.html';
            $scope.modalTitle = "New project";
            $scope.modalConfirm = "Create project";
            $scope.modalClick = function () {
                var name = $('#myModal input[name=name]').val(),
                    project_path = ($('#myModal input[name=project_path]').val()),
                    migration_path = (project_path+'sources-original/'),
                    tpl_path = (project_path+'usecase/'),
                    gen_path = (project_path+'generated/');

                var project = {
                    name:name,
                    project_path: project_path,
                    migration_path: migration_path,
                    tpl_path: tpl_path,
                    gen_path: gen_path
                };
                console.log('Creating project ', project);
                Project.new(project).then(function (result) {

                    console.log('Opening project with ID ', result.data.project_id);
                    Project.open(result.data.project_id).then(function (result) {

                        console.log('Project opened ', result.data);

                        var project_path_pub = result.data.project_path.replace(window.userPath, '');
                        $('.fs .header p').text('Folder '+ project_path_pub);
                        $('.jstree').jstree(true).settings.core.data.url = '/api/tree?dir='+encodeURIComponent(result.data.project_path);
                        $('.jstree').jstree(true).refresh(-1);
                        $('#myModal').modal('hide');

                        Console.disconnect();
                        Console.connect(result.data.id);
                        $scope.$emit('onUsecaseInit', result.data);

                    });

                });

            };


            $timeout(function () {
                $('#myModal').on('shown.bs.modal', function (e) {
                    if($('#myModal input[name=name]')) {
                        $('#myModal input[name=name]').focus();
                        $('#myModal input[name=name]').select();
                        $('#myModal input[name=name]').keypress(function (e) {
                            if (e.which == 13) {
                                $scope.modalClick();
                            }
                        });
                    }
                });
            });

            $timeout(function () {
                $('#myModal').modal('show');
                
                // $('#myModal input[name=name]').on
            });
        };




        $scope.openProjectModal = function () {

            Project.getAll().then(function (projects) {
                for (var i in projects.data) {
                    projects.data[i].project_path_pub = projects.data[i].project_path.replace(window.userPath, '');
                }
                $scope.projects = projects.data;

                $scope.modalSource = 'tpl/open-project.html';
                $scope.modalTitle = "Open project";
                $scope.modalConfirm = "Open";
                $scope.modalClick = function () {

                    var project_id = $('.open-project.active').data('id');

                    console.log('Opening project with ID ', project_id);
                    if (project_id) {
                        Project.open(project_id).then(function (result) {

                            console.log('Project opened ', result.data);

                            window.project = {id: project_id};

                            var project_path_pub = result.data.project_path.replace(window.userPath, '');
                            $('.fs .header p').text('Folder '+ project_path_pub);
                            $('.jstree').jstree(true).settings.core.data.url = '/api/tree?dir='+encodeURIComponent(result.data.project_path);
                            $('.jstree').jstree(true).refresh(-1);
                            $('#myModal').modal('hide');

                            Console.disconnect();
                            Console.connect(project_id);
                            $scope.$emit('onUsecaseInit', result.data);
                        });
                    } else {
                        alert('Please, choose a project')
                    }

                };

                $timeout(function () {
                    $('#myModal').modal('show');
                });
            })


        };

        $scope.$on('openProjectModal', function(event, data) {
            $scope.openProjectModal();
        });


        $scope.$on('doOpen3DUsecaseModal', function(event, data) {

            console.log(data.reload);
            if (!data.reload) {

                $scope.usecases = data.usecases;

                $scope.modalSource = 'tpl/open-3d-usecase.html';
                $scope.modalTitle = "Open use cases in 3D";
                $scope.modalConfirm = "Open";
                $scope.modalClick = function () {

                    var usecases_selected = [];
                    $('#myModal .modal-body .active').each(function () {
                        usecases_selected.push($(this).attr('data-id'));
                    });

                    window.ThreeD = {usecases_selected: usecases_selected};

                    Usecase.get3D(window.project.id, usecases_selected).then(function (res) {
                        $('#myModal').modal('hide');
                        
                        var usecases = res.data.usecases,
                            relationships = res.data.relationships;

                        draw3dview(usecases, relationships);
                    });
                    


                };

                $timeout(function () {
                    $('#myModal').modal('show');
                });
            } else {

                if (window.ThreeD.usecases_selected) {

                    var usecases_selected = window.ThreeD.usecases_selected;

                    Usecase.get3D(window.project.id, usecases_selected).then(function (res) {
                        var usecases = res.data.usecases,
                            relationships = res.data.relationships;

                        draw3dview(usecases, relationships);
                    });
                }
            }


        });

    }])


    .controller('UsecaseCtrl', ['$scope', 'Usecase', '$timeout', function ($scope, Usecase, $timeout) {

        $scope.usecases = [];
        $scope.$on('doOnUsecaseInit', function (event, data) {
            Usecase.getAll(data.id).then(function (data) {
                $scope.usecases = data.data;


                $timeout(function () {
                    $('.usecase-browser').find('ul').hide();

                    $('.usecase-browser').find('.toggle,.title').click(function() {
                        var li = $(this).parent();
                        li.find('> ul').slideToggle(0, function () {
                        });
                        if (li.find('> .toggle > .glyphicon').hasClass('glyphicon-plus')) {
                            li.find('> .toggle > .glyphicon').removeClass('glyphicon-plus');
                            li.find('> .toggle > .glyphicon').addClass('glyphicon-minus');
                        } else {
                            li.find('> .toggle > .glyphicon').removeClass('glyphicon-minus');
                            li.find('> .toggle > .glyphicon').addClass('glyphicon-plus');
                        }
                    });
                    //$('.usecase-browser > div > div > div > li > .title').click();
                });
            })
        });

    }])

    .controller('ThreeDCtrl', ['$scope', 'Usecase', '$timeout', function ($scope, Usecase, $timeout) {
        
        $scope.usecaseReload = function () {
            $scope.usecaseOpen(true);
        }

        $scope.usecaseOpen = function (reload) {

            var project_id = false; 
            if (window.project && window.project.id) {
                var project_id = window.project.id;
            }

            if (!project_id) {
                alert('Please, open a project')
                return;
            }

            Usecase.getAll(project_id).then(function (data) {

                $scope.$emit('open3DUsecaseModal', {usecases: data.data, reload: reload});

            })
        };
    }])

    .controller('ConsoleCtrl', ['$scope', 'Console', '$sce', '$timeout', function ($scope, Console, $sce, $timeout) {
        $scope.Console = Console;
        $scope.trustAsHtml = function (str) {
            return $sce.trustAsHtml(str);
        };


        $scope.$watchCollection('Console.collection', function(newCol, oldCol, scope) {
            $timeout(function () {
                var scrollPane = $('.console-browser').jScrollPane().data('jsp');
                scrollPane.scrollToBottom();
            });
        });
    }])

    .controller('AppCtrl', ['$scope', '$timeout', 'Inspector', function($scope, $timeout, Inspector) {

        $scope.tabs = [];

        var tabs = $( "#tabs" ).tabs();
        var firstPanel = 50,
            secondPanel = 50;
//            thirdPanel = 33.3;

        $scope.hideFs = function () {
            if ($('#content .fs').css('z-index') == '5') {

            } else {

                $('#content .fs').css('z-index', '5');
                $('#content .inspector').css('z-index', '4');
                $('#content .usecase').css('z-index', '4');
            }
        };
        $scope.hideInspector = function () {
            if ($('#content .inspector').css('z-index') == '4') {
                $('#content .inspector').css('z-index', '5');
                $('#content .fs').css('z-index', '4');
                $('#content .usecase').css('z-index', '4');
            } else {

            }
        };
        $scope.hideUsecase = function () {
            if ($('#content .usecase').css('z-index') == '4') {
                $('#content .usecase').css('z-index', '5');
                $('#content .fs').css('z-index', '4');
                $('#content .inspector').css('z-index', '4');
            } else {

            }
        };
        $scope.hideConsole = function () {
            if ($('#content .console').css('right') == '0px') {
                $('#content .console').css('right', '-300px');
                $('#content .file-viewer .tab-pane').css('padding-right', '0px');
            } else {
                $('#content .console').css('right', '0px');
                $('#content .file-viewer .tab-pane').css('padding-right', '300px');
            }
        };

        // hide all by default
        $scope.hideConsole();
        $('#content .fs .flag').hide();
        $('#content .console .flag').hide();
        $('#content .inspector').hide();
        $('#content .inspector .flag').hide();
        $('#content .usecase').hide();
        $('#content .usecase .flag').hide();


        $scope.$on('hideConsole', function(event, o) {
            $scope.hideConsole();
        });

        $scope.$on('twoView', function(event, refresh) {
            $scope.twoView(refresh);
        });

        $scope.twoView = function (refresh) {

            if (refresh == 'refresh' || !$('#content').hasClass('twoView')) {

                //$('#content .fs .flag').show();
                //$('#content .console .flag').show();
                $('#content .inspector').show();
//                $('#content .usecase').show();

//                $('#content .usecase').css('height', 'calc('+thirdPanel+'% - 31px + 31px - 16px)');
//                $('#content .usecase').css('top', 'calc('+secondPanel+'% + '+firstPanel+'% + 30px  - 16px)');
                $('#content .inspector').css('height', 'calc('+secondPanel+'% - 31px  + 15px)');
                $('#content .inspector').css('top', 'calc('+firstPanel+'% + 30px)');
                $('#content .fs').css('height', 'calc('+firstPanel+'%)');
                $('#content .fs').css('top', '31px');
                $('#content').addClass('twoView');


                function dragy(panel) {
                    var start,
                        stop,
                        max_percent=$(window).height();

                    $( "."+panel+" .header" ).draggable({
                        axis: "y",
                        //revert : function(event, ui) {
                        //    return false;
                        //    // on older version of jQuery use "draggable"
                        //    // $(this).data("draggable")
                        //    // on 2.x versions of jQuery use "ui-draggable"
                        //    // $(this).data("ui-draggable")
                        //
                        //    $(this).data("ui-draggable").originalPosition = {
                        //        top : 0,
                        //        left : 0
                        //    };
                        //    // return boolean
                        //    return !event;
                        //    // that evaluate like this:
                        //    // return event !== false ? false : true;
                        //},
                        start: function(event, ui) {

                            if (panel == 'usecase') {
                                $scope.hideUsecase();
                            } else if (panel == 'inspector') {
                                $scope.hideInspector();
                            }
                            start = ui.position.top;
                        },
                        stop: function(event, ui) {
                            stop = ui.position.top;
                            var dif = start-stop,
                                percent = Math.round(Math.abs(dif)*100/max_percent),
                                dir;
                            if (dif > 0) {
                                dir = 'top';
                            } else {
                                dir = 'bottom';
                            }
                            if (dir == 'top') {
                                if (panel == 'usecase') {
                                    thirdPanel+=percent;
                                    secondPanel-=percent;
                                } else if (panel == 'inspector') {
                                    secondPanel+=percent;
                                    firstPanel-=percent;
                                } else if (panel == 'fs') {

                                }
                                $scope.twoView('refresh');
                            } else if (dir == 'bottom') {

                                if (panel == 'usecase') {
                                    thirdPanel -= percent;
                                    secondPanel += percent;
                                } else if (panel == 'inspector') {
                                    secondPanel -= percent;
                                    firstPanel += percent;
                                } else if (panel == 'fs') {

                                }
                                $scope.twoView('refresh');
                            }

                            $(this).css('top', 0);
                            $(this).css('left', 0);
                        }
                    });
                }

                dragy('usecase');
                dragy('inspector');
                //drag('fs');

            } else {
//                $('#content .usecase').css('height', 'calc(100% - 31px)');
//                $('#content .usecase').css('top', '30px');
                $('#content .inspector').css('height', 'calc(100% - 31px)');
                $('#content .inspector').css('top', '30px');
                $('#content .fs').css('height', 'calc(100% - 31px)');
                $('#content .fs').css('top', '31px');
                $('#content').removeClass('twoView');

                $('#content .fs').show();
                $('#content .fs .flag').hide();
                $('#content .console .flag').hide();
                $('#content .inspector').hide();
//                $('#content .usecase').hide();
                firstPanel = 34;
                secondPanel = 33;
//                thirdPanel = 33;
            }
        };

        // toggle twoview
        $scope.twoView();

        $scope.removeTab = function (tabI) {
            $scope.tabs.splice(tabI, 1);
        };

        $scope.$on('code', function(event, data) {

            function basename(path) {
                return path.split('/').reverse()[0];
            }

            data['basename'] = basename(data.path);

            var found = false;
            var i = 0;

            for (i in $scope.tabs) {
                if ($scope.tabs[i].path == data.path) {
                    $scope.tabs[i] = {
                        editor: $scope.tabs[i].editor,
                        path: data.path,
                        code: data.code,
                        basename: data.basename
                    };
                    found = true;
                    break;
                }
            }

            if (!found) {
                $scope.tabs[$scope.tabs.length] = data;
                i = $scope.tabs.length-1;
            }

            $timeout(function () {

                if (!found) {
                    tabs.tabs();
                    $('a[href="#tabs-' + i + '"]').on('shown.bs.tab', function (e) {
                        Inspector.inspect($(e.target).data('path')).then(function (inspected) {
                            $scope.$broadcast('inspected', inspected.data);
                        });
                    });
                }
                $('#tabs a[href="#tabs-' + i + '"]').tab('show');

                //// Sorting does not work with angular updates, somehow it breaks TODO
                //tabs.find( ".ui-tabs-nav" ).sortable({
                //    axis: "x",
                //    stop: function(event, ui) {
                //        tabs.tabs( "refresh" );
                //
                //        var target = $(event.target);
                //        var lis = target.find('li'), li;
                //        var tabs_before = angular.copy($scope.tabs);
                //
                //        target.find('li').each(function (index, el) {
                //            var k = parseInt($(el).data('i')),
                //                extension = $(el).data('basename').split('.').pop(),
                //                language = "text/html";
                //
                //            if (k!=index) {
                //                $scope.tabs[index] = tabs_before[k];
                //
                //                $timeout(function () {
                //                    tabs.tabs();
                //
                //                    if ($(el).hasClass('active')) {
                //                        $('#tabs a[href="#tabs-' + index +'"]').tab('show');
                //                    }
                //
                //                    switch (extension) {
                //                        case 'xml': language = 'xml'; break;
                //                        case 'md': language = 'markdown'; break;
                //                        case 'php': language = 'php'; break;
                //                        case 'py': language = 'python'; break;
                //                        case 'rb': language = 'ruby'; break;
                //                        case 'sql': language = 'sql'; break;
                //                        case 'json': language = 'javascript'; break;
                //                        case 'js': language = 'javascript'; break;
                //                        case 'css': language = 'css'; break;
                //                        case 'html': language = 'htmlmixed'; break;
                //                    }
                //
                //                    // after render complete
                //
                //                    CodeMirror.fromTextArea(document.getElementById("code-" + index), {
                //                        lineNumbers: true,
                //                        mode: language,
                //                        matchBrackets: true,
                //                        viewportMargin: Infinity,
                //                        theme: 'monokai'
                //                    });
                //                });
                //            }
                //
                //        });
                //    }
                //});


                if (!found) {
                    var extension = data['basename'].split('.').pop(),
                        language = "text/html";

                    switch (extension) {
                        case 'xml':
                            language = 'xml';
                            break;
                        case 'md':
                            language = 'markdown';
                            break;
                        case 'php':
                            language = "application/x-httpd-php";
                            break;
                        case 'py':
                            language = 'python';
                            break;
                        case 'rb':
                            language = 'ruby';
                            break;
                        case 'sql':
                            language = 'sql';
                            break;
                        case 'json':
                            language = 'javascript';
                            break;
                        case 'js':
                            language = 'javascript';
                            break;
                        case 'css':
                            language = 'css';
                            break;
                        case 'html':
                            language = 'htmlmixed';
                            break;
                    }

                    var editor = CodeMirror.fromTextArea(document.getElementById("code-" + i), {
                        lineNumbers: true,
                        mode: language,
                        matchBrackets: true,
                        viewportMargin: Infinity,
                        theme: 'monokai'
                    });
                    $scope.tabs[i].editor = editor;


                    var api = $("#tabs-"+i).jScrollPane().data('jsp');
                    $(window).on('resize', function(){
                        api.reinitialise();
                    });
                    window['jScrollPane']["#tabs-"+i] = function () {
                        api.reinitialise();
                    };

                    if(parseInt(localStorage.getItem("ScrollPosition"+i)) > 0) {
                        api.scrollToY(parseInt(localStorage.getItem("ScrollPosition"+i)))
                    }
                    $("#tabs-"+i).bind("jsp-scroll-y", function(event, scrollPositionY, isAtTop, isAtBottom) {
                        localStorage.setItem("ScrollPosition"+i, scrollPositionY)
                    }).jScrollPane();


                    editor.on("change", function(instance, changeObj) {
                        var i = $(instance.getTextArea()).closest('.tab-pane').data('i');
                        $('.file-tab'+i).removeClass('saved');
                        $('.file-tab'+i).addClass('unsaved');

                        var curTime = new Date().getTime();
                        if ('jScrollPane-t' in window && (curTime - window['jScrollPane-t']) < 1000 ) {
                            clearTimeout(window['jScrollPane-c']);
                        }
                        window['jScrollPane-t'] = new Date().getTime();
                        window['jScrollPane-c'] = setTimeout(function(){ window['jScrollPane']["#tabs-"+i](); }, 1000);;

                    });


                }

                window['jScrollPane']["#tabs-"+i]();
            });



            Inspector.inspect(data.path).then(function (inspected) {
                $scope.$broadcast('inspected', inspected.data);
            });

        });

        // update jScroolPane when panel is clicked
        $('.inspector .nav-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            if ($(this).attr('href') === '#inspector') {
                window['jScrollPane']['.inspect-browser']();
            }
            if ($(this).attr('href') === '#console') {
                window['jScrollPane']['.console-browser']();
            }
        });

        $scope.$on('saveFileClicked', function () {
            $scope.saveFile();
        });


        $scope.$on('onUsecaseInit', function (event, data) {
            console.log('broadcasting doOnUsecaseInit');
            $scope.$broadcast('doOnUsecaseInit', data);
        });


        $scope.$on('open3DUsecaseModal', function (event, data) {
            $scope.$broadcast('doOpen3DUsecaseModal', data);
        });

        $scope.saveFile = function () {

            if ($('.file-tab.active').length === 1) {
                var i = $('.file-tab.active').data('i'),
                    path = $('.file-tab.active > a').data('path'),
                    value = $scope.tabs[i].editor.getValue();

                $scope.$broadcast('saveFile', {
                    i:i,
                    path:path,
                    value:value,
                    done: function () {
                        Inspector.inspect(path).then(function (inspected) {
                            $scope.$broadcast('inspected', inspected.data);
                        });
                    }
                });
            }
        };


        $(window).bind('keydown', function(event) {
            if (event.ctrlKey || event.metaKey) {
                switch (String.fromCharCode(event.which).toLowerCase()) {
                    case 's':
                        event.preventDefault();
                        $scope.saveFile();
                        break;

                    case 'o':
                        event.preventDefault();

                        $scope.$broadcast('openProjectModal', {});
                        break;
                }
            }
        });



    }])


    .controller('InspectorCtrl', ['$scope', '$sce', '$timeout', function ($scope, $sce, $timeout) {

        //$scope.inspected = $sce.trustAsHtml('<p>None</p>');
        $scope.inspected = [];
        $scope.$on('inspected', function(event, data) {
            //function nodeToString ( node ) {
            //    var tmpNode = document.createElement( "div" );
            //    tmpNode.appendChild( node.cloneNode( true ) );
            //    var str = tmpNode.innerHTML;
            //    tmpNode = node = null; // prevent memory leaks in IE
            //    return str;
            //}
            //$scope.inspected = $sce.trustAsHtml(nodeToString(prettyPrint(data, {
            //    maxArray: 20, // Set max for array display (default: infinity)
            //    expanded: true, // Expanded view (boolean) (default: true),
            //    maxDepth: 5 // Max member depth (when displaying objects) (default: 3)
            //})));
            $scope.inspected = data;

            $timeout(function () {
                $('.inspect-browser').find('ul').hide();

                $('.inspect-browser').find('.toggle,.title').click(function() {
                    var li = $(this).parent();
                    li.find('> ul').slideToggle(0, function () {
                    });
                    if (li.find('> .toggle > .glyphicon').hasClass('glyphicon-plus')) {
                        li.find('> .toggle > .glyphicon').removeClass('glyphicon-plus');
                        li.find('> .toggle > .glyphicon').addClass('glyphicon-minus');
                    } else {
                        li.find('> .toggle > .glyphicon').removeClass('glyphicon-minus');
                        li.find('> .toggle > .glyphicon').addClass('glyphicon-plus');
                    }
                    window['jScrollPane']['.inspect-browser']();
                });
                $('.inspect-browser > div > div > div > li > .title').click();

            });

        });


    }])

    .controller('FilebrowserCtrl', ['$scope', 'FetchFileFactory', function ($scope, FetchFileFactory) {

        $scope.fileViewer = 'Please select a file to view its contents';

        $scope.$on('saveFile', function (event, data) {

            FetchFileFactory.saveFile(data).then(function () {
                $('.file-tab'+data.i).removeClass('unsaved');
                $('.file-tab'+data.i).addClass('saved');
                data.done();
            });

        });

        $scope.openNode = function(e, data) {
            window['jScrollPane']['.fs-browser']();
        };

        $scope.loadNode = function(e, data) {
            //console.log(e.currentTarget, data);
            $(e.currentTarget).find("li").contextMenu({
                menuSelector: "#contextMenu",
                menuSelected: function (invokedOn, selectedMenu) {
                    var path = invokedOn.closest('.jstree-node').attr('base'),
                        selected = selectedMenu.data('menu-id');

                    switch (selected) {
                        case 'new-file':
                            $('#new-file').modal({ backdrop: 'static', keyboard: false })
                                .one('click', '#create', function() {
                                    FetchFileFactory.createFile({path:path, filename:$('#new-file-name').val(), folder: false}).then(function () {
                                        $('.jstree').jstree('refresh_node', invokedOn);
                                        window['jScrollPane']['.fs-browser']();
                                    });
                                });

                            break;
                        case 'new-folder':
                            $('#new-file').modal({ backdrop: 'static', keyboard: false })
                                .one('click', '#create', function() {
                                    FetchFileFactory.createFile({path:path, filename:$('#new-file-name').val(), folder: true}).then(function () {
                                        $('.jstree').jstree('refresh_node', invokedOn);
                                        window['jScrollPane']['.fs-browser']();
                                    });
                                });

                            break;
                        case 'rm':
                            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                                .one('click', '#delete', function() {
                                    FetchFileFactory.removeFile({path:path}).then(function () {
                                        $('.jstree').jstree('refresh_node', invokedOn.parent().parent());
                                        window['jScrollPane']['.fs-browser']();
                                    });
                                });
                            break;
                        case 'refresh':
                            $('.jstree').jstree('refresh_node', invokedOn);
                            window['jScrollPane']['.fs-browser']();
                            break;
                    }
                }
            });
        };

        $scope.nodeSelected = function(e, data) {
            var _l = data.node.li_attr;
            if (_l.isLeaf) {
                FetchFileFactory.fetchFile(_l.base).then(function(data) {
                    var _d = data.data;
                    if (typeof _d == 'object') {

                        //http://stackoverflow.com/a/7220510/1015046//
                        _d = JSON.stringify(_d, undefined, 2);
                    }
                    $scope.fileViewer = _d;

                    $scope.$emit('code', {
                        path: _l.base,
                        code: _d
                    });

                });
            } else {


            }


        };

    }])






;