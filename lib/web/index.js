var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');

var web = express();
var expressWs = require('express-ws')(web);
var Project = require('_/app/project');


var session = require('express-session');

web.use(session({
  secret: 'awo83yxmo',
  resave: false,
  saveUninitialized: true
}));

module.exports = function (application, db) {


  // view engine setup
  web.set('views', path.join(__dirname, 'views'));
  web.set('view engine', 'ejs');

  // uncomment after placing your favicon in /public
  //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
  web.use(logger('dev'));
  web.use(bodyParser.json());
  web.use(bodyParser.urlencoded({ extended: false }));
  web.use(cookieParser());
  web.use(require('node-compass')({ mode: 'expanded', project: path.join(__dirname, 'public') }));
  web.use(express.static(path.join(__dirname, 'public')));

  web.use(function(req, res, next) {
    req.application   = application;
    req.db            = db;
    req.models        = db.models;
    next();
  });

  function make_observer(websocket) {
    return function (obj) {
      try {
        websocket.send(obj);
      } catch (e) {
        console.log(e);
      }
    }
  }
  web.ws('/data', function(ws, req) {
    ws.on('message', function(msg) {
      var msg_obj = JSON.parse(msg),
          project_id = msg_obj.project_id,
          project = new Project(req.application, req.models);

      project.registerObserver(project_id, make_observer(ws), function (res) {
        console.log('register observer result ', res);
      });
    });
    ws.send(JSON.stringify({ready:true,message:"Ready.\n"}));
  });
  web.use('/', routes);

  // catch 404 and forward to error handler
  //web.use(function(req, res, next) {
  //  var err = new Error('Not Found');
  //  err.status = 404;
  //  next(err);
  //});

  // error handlers

  // development error handler
  // will print stacktrace
  //if (web.get('env') === 'development') {
  //  web.use(function(err, req, res, next) {
  //    res.status(err.status || 500);
  //    res.render('error', {
  //      message: err.message,
  //      error: err
  //    });
  //  });
  //}
  //
  //// production error handler
  //// no stacktraces leaked to user
  //web.use(function(err, req, res, next) {
  //  res.status(err.status || 500);
  //  res.render('error', {
  //    message: err.message,
  //    error: {}
  //  });
  //});

  return web;
};
