var express = require('express');
var bodyParser     =        require("body-parser");
var router = express.Router();
router.use(bodyParser.urlencoded({ extended: false }));
var fs = require('fs');
var path = require('path'),
  fstools = require('_/app/generator/fstools'),
  parser = require('_/parser'),
  Project = require('_/app/project'),
  Promise = require('promise');



function filter(orig_f) {
  return function(req, res, next) {
    if (!req.session.user) {res.redirect('/');return;}
    return orig_f(req, res, next);
  }
}

/* GET home page. */
router.get('/', function(req, res, next) {
  if (req.session.user) {
    res.redirect('/editor.html');
    return;
  }
  res.render('index', { title: 'Express' });
});

router.get('/logout.json', function(req, res) {
  req.session.user = null;
  res.redirect('/');
});

router.post('/login.json', function(req, res) {

  var users = [
    {
      username: 'valentino',
      password: 'vranic',
      name: 'Valentino',
      photo: 'http://www2.fiit.stuba.sk/~vranic/img/vranic.jpg'
    },
    {
      username: 'dash',
      password: 'dash',
      name: 'Michal',
      photo: '/images/dash.jpg'
    }
  ];

  var username = req.body.username,
      password = req.body.password;

  for (var i in users) {
    if (users[i].username == username && users[i].password == password) {

      var user = users[i];
      req.session.user = user;

      fstools.mkdirpAsync('./var/'+user.username+'/projects/', function () {
        res.send({username: user.username, name: user.name, photo: user.photo});
      });

      return;
    }
  }

  res.send({'error': 'Bad username or password'});

});

router.get('/editor.html', filter(function(req, res, next) {

  res.render('editor', { user: req.session.user });
}));

router.get('/3dview.html', filter(function(req, res, next) {
  res.render('3dview', { user: req.session.user });
}));

/* Serve the Tree */
router.get('/api/tree', filter(function(req, res) {
  var _p,
      dir = req.query.dir,
      user = req.session.user;
  if (req.query.id == 1) {
    //_p = path.resolve(__dirname, '..', '..', '..', user.username, 'projects');
    _p = dir;
    processReq(_p, res);

  } else {
    if (req.query.id) {
      _p = req.query.id;
      console.log(_p);
      processReq(_p, res);
    } else {
      res.json(['No valid data found']);
    }
  }
}));

/* Serve a Resource */
router.get('/api/resource', filter(function(req, res) {
  res.send(fs.readFileSync(req.query.resource, 'UTF-8'));
}));

router.get('/api/inspect', filter(function(req, res) {
  var path_i = req.query.resource,
      fileData = fs.readFileSync(path_i, 'UTF-8'),
      lang = path_i.substr(path_i.lastIndexOf(".") + 1),
      classes_tree = parser.code.analyse(fileData, lang);
  res.send(classes_tree);
}));


router.post('/api/project/new', filter(function(req, res) {
  var name = req.body.name,
      user = req.session.user,
      project_path = ('./var/'+user.username+'/'+req.body.project_path)     .replace('/./', '/'),
      migration_path = ('./var/'+user.username+'/'+req.body.migration_path) .replace('/./', '/'),
      tpl_path = ('./var/'+user.username+'/'+req.body.tpl_path)             .replace('/./', '/'),
      gen_path = ('./var/'+user.username+'/'+req.body.gen_path)             .replace('/./', '/'),
      project = new Project(req.application, req.db);

  project.new({
    name: name,
    user: user.username,
    project_path: project_path,
    migration_path: migration_path,
    tpl_path: tpl_path,
    gen_path: gen_path
  }, function (project_id) {
    res.send(project_id);
  });

}));
router.post('/api/project/open', filter(function(req, res) {
  var project_id = req.body.project_id,
      project = new Project(req.application, req.db);

  project.open(project_id, function (result) {
    res.send(result);
  });

}));


router.get('/api/project/get_all', filter(function(req, res) {
  var project = new Project(req.application, req.db);

  project.getAll(req.session.user.username, function (result) {
    res.send(result);
  });

}));

function compare(a,b) {
  if (a.no < b.no)
    return -1;
  if (a.no > b.no)
    return 1;
  return 0;
}

var UsecasePresenter = function (usecase) {

  // step types - trigger, step, condition, precondition, postcondition
  var triggers = [], steps = [], precondition = [], postcondition = [];
  for (var i in usecase.scenario.steps) {
    var step = usecase.scenario.steps[i];
    switch (step.step_type.name) {
      case 'trigger':
        triggers.push(step);
        break;
      case 'step':
        steps.push(step);
        break;
      case 'condition':
        steps.push(step);
        break;
      case 'precondition':
        precondition.push(step);
        break;
      case 'postcondition':
        postcondition.push(step);
        break;
    }
  }
  triggers.sort(compare);
  steps.sort(compare);
  precondition.sort(compare);
  postcondition.sort(compare);

  usecase.scenario.triggers = triggers;
  usecase.scenario.steps = steps;
  usecase.scenario.precondition = precondition;
  usecase.scenario.postcondition = postcondition;

  return usecase;

};

router.post('/api/usecase/get_all', filter(function(req, res) {
  var project_id = req.body.project_id,
      Usecase = req.models.usecase;

  Usecase.find({project_id: parseInt(project_id)}, function (err, usecases) {
    if (err) throw err;

    var scenarios = [];

    for (var i in usecases) {
      function get_scenario(i) {
        return new Promise(function (fulfill, reject) {
          usecases[i].getScenario(function (err, scenario) {
            if (err) throw err;
            usecases[i].scenario = scenario;

            scenario.getStep(function(err, steps) {
              if (err) throw err;
              usecases[i].scenario.steps = steps;

              fulfill();
            });

          });
        });
      }
      scenarios.push(get_scenario(i));
    }

    Promise.all(scenarios).then(function () {
      for (var i in usecases) {
        usecases[i] = UsecasePresenter(usecases[i]);
      }
      res.send(usecases);
    });
  });

}));


router.post('/api/usecase/get_3d', filter(function(req, res) {
  var usecases_selected = req.body.usecases_selected,
      project_id = req.body.project_id,
      Usecase = req.models.usecase,
      Relationship = req.models.relationship;


  var find_query_rel;
  if (usecases_selected[0] == 'all') {
      find_query_rel = {};
  } else {
      find_query_rel = {or: [{usecase_id: usecases_selected}, {related_usecase_id: usecases_selected}]};
  }

  Relationship.find(find_query_rel, function (err, relationships) {
    if (err) throw err;

    for (var i in relationships) {
      usecases_selected.push(relationships[i].usecase_id);
      usecases_selected.push(relationships[i].related_usecase_id)
    }

    var find_query;
    if (usecases_selected[0] == 'all') {
      find_query = {project_id: parseInt(project_id)};
    } else {
      find_query = {id: usecases_selected};
    }

    Usecase.find(find_query, function (err, usecases) {
      if (err) throw err;

      var scenarios = [];

      for (var i in usecases) {
        function get_scenario(i) {
          return new Promise(function (fulfill, reject) {
            usecases[i].getScenario(function (err, scenario) {
              if (err) throw err;
              usecases[i].scenario = scenario;

              scenario.getStep(function(err, steps) {
                if (err) throw err;
                usecases[i].scenario.steps = steps;

                fulfill();
              });

            });
          });
        }
        scenarios.push(get_scenario(i));
      }

      Promise.all(scenarios).then(function () {
        for (var i in usecases) {
          usecases[i] = UsecasePresenter(usecases[i]);
        }
        res.send({usecases: usecases, relationships: relationships});
      });
    });

  })

}));

router.post('/api/file/create', filter(function(req, res) {
  var path_i = req.body.path,
      filename = req.body.filename,
      folder = req.body.folder;

    if (fs.lstatSync(path_i).isDirectory()) {

      if (folder) {
        fs.mkdir(path_i + '/' + filename, function (err) {
          if (err) throw err;
          res.send({'error': null});
        });
      } else {
        fs.writeFile(path_i + '/' + filename, ' ', function (err) {
          if (err) throw err;
          res.send({'error': null});
        });
      }
    }
}));

router.post('/api/file/save', filter(function(req, res) {
  var path_i = req.body.path,
      value = req.body.value;

  fs.writeFile(path_i, value, function (err) {
    if (err) throw err;
    res.send({'error': null});
  })
}));

router.post('/api/file/rm', filter(function(req, res) {
  var path_i = req.body.path;

  if (fs.lstatSync(path_i).isFile()) {
    fs.unlink(path_i, function (err) {
      if (err) throw err;
      res.send({'error': null});
    })
  } else {
    fstools.removeFolderRecursiveAsync(path_i, function () {
      res.send({'error': null});
    });
  }
}));



function processReq(_p, res) {
  var resp = [];
  fs.readdir(_p, function(err, list) {
    if (list)
      for (var i = list.length - 1; i >= 0; i--) {
        resp.push(processNode(_p, list[i]));
      }
    res.json(resp);
  });
}

function processNode(_p, f) {
  var s = fs.statSync(path.join(_p, f));
  return {
    "id": path.join(_p, f),
    "text": f,
    "icon" : s.isDirectory() ? 'jstree-custom-folder' : 'jstree-custom-file',
    "state": {
      "opened": false,
      "disabled": false,
      "selected": false
    },
    "li_attr": {
      "base": path.join(_p, f),
      "isLeaf": !s.isDirectory()
    },
    "children": s.isDirectory()
  };
}



module.exports = router;
