
var parser = require('_/parser'),
    colors = require('colors'),
    // disable colors
    //colors = {
    //    blue: function (str) { return str; },
    //    yellow: function (str) { return str; },
    //    magenta: function (str) { return str; },
    //    cyan: function (str) { return str; },
    //    white: function (str) { return str; },
    //    green: function (str) { return str; },
    //    red: function (str) { return str; },
    //    grey: function (str) { return str; },
    //    bold: function (str) { return str; },
    //},
    formatDate = function (date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        hours = hours < 10 ? '0'+hours : hours;
        minutes = minutes < 10 ? '0'+minutes : minutes;
        seconds = seconds < 10 ? '0'+seconds : seconds;
        var strTime = hours + ':' + minutes + ':' + seconds + ' ';
        return strTime;
//        return date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear() + " " + strTime;
    };

module.exports = function () {

    this.log = console.log.bind(console);

    this.registerObserver = function () {

    };

    this.logUsecaseChanges = function (usecase, level, changes) {
        var message = this.logUsecaseChangesIn(usecase, level, changes);
        this.log (message);
    };

    this.logUsecaseChangesIn = function (usecase, level, changes) {

        var message = "";
        if (usecase !== null) {

            var spaces_str = "";
            for (var k = 0; k < usecase.name.length; k++) {
                spaces_str += "-";
            }
            message += spaces_str+"\n"+usecase.name+"\n"+spaces_str+"\n"+"\n";
        }

        var spaces = 2,
            levels = [
                colors.blue,
                colors.yellow,
                colors.magenta,
                colors.cyan,
                colors.blue,
                colors.yellow,
                colors.magenta,
                colors.cyan
            ];

        for (var i in changes) {
            var key = i,
                spaces_str = "";

            for (var k = 0; k < spaces*level; k++) {
                spaces_str += " ";
            }


            // go through
            if (Object.prototype.toString.call(changes[i]) == "[object Object]") {

                message += spaces_str + levels[level](key+":") + "\n";
                message += this.logUsecaseChangesIn(null, level+1, changes[i]) + "\n";

            } else if (Object.prototype.toString.call(changes[i]) === "[object Array]") {

                message += spaces_str + levels[level](key+":") + "\n";
                message += this.logUsecaseChangesIn(null, level+1, changes[i]) + "\n";

            } else {

                message += spaces_str + levels[level](key+":\t") + changes[i] + "\n";
            }
        }

        return message;
    };

    this.logFileDiffs = function (diffs) {
        var message = this.logFileDiffsIn(diffs);
        this.log (message);
    };

    this.logFileDiffsIn = function (diffs) {

        var message = "";

        for (var i in diffs) {
            var path = diffs[i].path,
                diff = diffs[i].diff,
                spaces_str = "";

            for (var k = 0; k < path.length; k++) {
                spaces_str += "-";
            }
            message += spaces_str+"\n"+path+"\n"+spaces_str+"\n" + "\n";

            for (var j in diff) {
                if (diff[j].added) {
                    var lines = diff[j].value.split("\n");

                    for (var k in lines) {
                        message += colors.green("+ ") + colors.white(lines[k]) + "\n";
                    }
                }
                if (diff[j].removed) {
                    var lines = diff[j].value.split("\n");

                    for (var k in lines) {
                        message += colors.red("- ") + colors.white(lines[k]) + "\n";
                    }
                }
            }
            message += "\n";
        }

        return message;
    };


    this.logFileChange = function (path, info) {
        var message = this.logFileChangeIn(path, info);
        this.log (message);
    };

    this.logFileChangeIn = function (path, info) {

        var now = new Date(),
            short_path  = path,
                //.replace(cfg.usecase_path+'/', '')
                //.replace(cfg.tpl_path+'/', ''),
            message     =
                short_path + ' '+info+'   ' + colors.grey(formatDate(now)) + ' ';


        var len = 60,
            add = (len-message.length),
            spaces_str = " ";
        for (var k = 0; k < add; k++) {
            spaces_str += "-";
        }
        message = colors.bold(message+colors.grey(spaces_str));

        return message;
    };
};