
var HtmlLogger = require('./loggers/html'),
    ConsoleLogger = require('./loggers/console');

module.exports = function (to_log) {

    this.loggers = [];

    if (to_log.indexOf('html') >= 0) {
        this.loggers.push(new HtmlLogger());
    }
    if (to_log.indexOf('console') >= 0) {
        this.loggers.push(new ConsoleLogger());
    }

    this.registerObserver = function (observer) {
        for (var i in this.loggers) {
            this.loggers[i].registerObserver(observer);
        }
    };

    this.logUsecaseChanges = function (usecase, level, changes) {
        for (var i in this.loggers) {
            this.loggers[i].logUsecaseChanges(usecase, level, changes)
        }
    };
    this.logFileDiffs = function (diffs) {
        for (var i in this.loggers) {
            this.loggers[i].logFileDiffs(diffs)
        }
    };
    this.logFileChange = function (path, info) {
        for (var i in this.loggers) {
            this.loggers[i].logFileChange(path, info)
        }
    };
};