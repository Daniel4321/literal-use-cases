
var fs      = require('fs'),
    regexps = require('../regexps.js'),
    utils   = require('../utils.js'),
    log     = console.log.bind(console),
    cleanName = function (name) {
        return name
            .replace(/[,\.:]/g, '')
            .replace(/[\s]*$/g, '')
            .replace(/^[\s]*/g, '');
    },


    Usecase = function (name) {
        this.name = name;
        this.specializes = null;
        this.description = null;
        this.level = null;

        this.scenario = null;
        this.actors = {};
        this.triggers = [];
        this.preconditions = [];
        this.postconditions = [];
        this.extension_points = {};

        // defacto ID
        this.path = null;
    },

    Scenario = function () {
        this.steps = {};

        this.set_step = function (n, step) {
            this.steps[n] = step;
        };
    },

    Actor = function (name) {
        this.name = cleanName(name);
        this.description = null;
    },

    Step = function (name) {
        this.type = 'step';
        this.no = null;
        this.name = cleanName(name);
        this.orig_name = null;
        this.played_by = null;
        this.args = [];
        this.inherited = false;
    },

    Condition = function (name) {
        this.type = 'condition';
        this.no = null;
        this.name = cleanName(name);
        this.orig_name = null;
        this.played_by = null;
        this.args = [];
        this.inherited = false;
    },

    args = {
        extract: function (string) {
            var name = string.replace(regexps.between_quotes(), '')
                    .replace(/^\s*|\s*$/g, '')
                    .replace(/[\s]+/g, ' '),

                args = string.match(regexps.between_quotes());

            for (var i in args) {
                args[i] =
                    args[i].replace(/\"/g, '');
            }

            return {
                name: name,
                args: args?args:[]
            }
        }
    }

    ;

module.exports = function () {

    this.sections = [
        "usecase",
        "actors",
        "triggers",
        "scenario",
        "extensions",
        "preconditions",
        "postconditions",
        "extension_points"
    ];

    this.system_actors = [
        "System"
    ];

    this.line_separator = " ";

    this.parseUsecase = function (path) {

        var data = fs.readFileSync(path, 'utf8'),
            match = regexps.comments().exec(data),
            lines = (match && match.length >= 1) ? match[0].split("\n") : [],

            section = null,
            prev_sect = null,
            section_line_no = 0,
            uc = null,
            actor = null,
            condition = null,
            step = null;

        for (i in lines) {
            var line = lines[i].replace(regexps.comments_trash(), '');

            // remove empty lines
            if (/^\s*$/.test(line)) continue;

            // check section and count line per section
            var s;
            prev_sect = section;
            section = (s = this.get_section(line)) ? s : section;
            if (section === prev_sect) section_line_no++; else section_line_no=1;

            switch (section) {
                case "usecase":
                    if (section_line_no === 1) {
                        if (regexps.uc_specializes().test(line)) {
                            var specializes = line.match(regexps.uc_specializes());

                            uc = new Usecase(line
                                .replace(regexps.uc_begin(), '')
                                .replace(regexps.uc_specializes(), ''));

                            uc.specializes = specializes[1].replace(/\s*$/, '');

                        } else {
                            uc = new Usecase(line.replace(regexps.uc_begin(), ''));
                        }
                        uc.path = path;
                        uc.level = path.split('/')[path.split('/').length-2];

                    } else {
                        uc.description =
                            this.append(line, uc.description);
                    }

                    break;
                case "actors":

                    if (uc === null) break;

                    if (section_line_no === 1)
                        continue;

                    if (regexps.actor_name().test(line)) {
                        var actor_name = regexps.actor_name().exec(line).toString().replace(/[\s]*:[\s]*$/, '');
                        actor = new Actor(actor_name);
                        uc.actors[actor.name] = actor;
                        actor.description = line.replace(regexps.actor_name(), '').replace(/^[\s]*/, '');
                    } else {
                        if (actor !== null)
                            actor.description =
                                this.append(line, actor.description);
                    }

                    break;

                case "extension_points":

                    if (uc === null) break;

                    if (section !== prev_sect) condition = null;

                    if (section_line_no === 1)
                        continue;

                    var a = args.extract(line.replace(new RegExp('^' + user + "\\s+"), ''));
                    var ar = a['args'];
                    if (ar.length >= 0)
                        uc.extension_points[ar[1]] = {"name": ar[0],"step_no": ar[1]};

                    break;

                case "triggers":
                case "preconditions":
                case "postconditions":

                    if (uc === null) break;

                    if (section !== prev_sect) condition = null;

                    if (section_line_no === 1)
                        continue;

                    var orig_name = line;

                    // inherited
                    var inherited = false;
                    if (regexps.step_inherited().test(line)) {
                        line = line.replace(regexps.step_inherited(), '');
                        inherited = true;
                    }

                    /*
                     * Check if there is a new condition by users
                     * if user variable is not null, there is a new condition by the user
                     */
                    var u,
                        user = null;
                    for (var a in uc.actors) {
                        u = a;
                        if (utils.startsWith(line, u))
                            user = u;
                    }

                    // check if there is a new condition by system
                    for (var i in this.system_actors) {
                        u = this.system_actors[i];
                        if (utils.startsWith(line, u))
                            user = u;
                    }

                    if (user !== null) {
                        // new condition by actor


                        // remove user from line
                        var t_name = args.extract(line.replace(new RegExp('^'+user+"\\s+"), ''));

                        condition = new Condition(t_name['name']);
                        condition.orig_name = orig_name;
                        condition.args = condition.args.concat(t_name['args']);
                        condition.played_by = user;
                        condition.inherited = inherited;

                        if (section === "triggers")
                            uc.triggers.push(condition);
                        else if (section === "preconditions")
                            uc.preconditions.push(condition);
                        else if (section === "postconditions")
                            uc.postconditions.push(condition);

                    } else {

                        var t_name = args.extract(line);

                        if (condition !== null) {
                            // existing condition written in multiple lines
                            condition.name =
                                this.append(t_name['name'], condition.name);
                            condition.orig_name = orig_name;
                            condition.args = condition.args.concat(t_name['args']);
                            condition.inherited = inherited;
                        }
                        else {
                            // condition without actor
                            condition = new Condition(t_name['name']);
                            condition.orig_name = orig_name;
                            condition.args = condition.args.concat(t_name['args']);
                            condition.inherited = inherited;

                            if (section === "triggers")
                                uc.triggers.push(condition);
                            else if (section === "preconditions")
                                uc.preconditions.push(condition);
                            else if (section === "postconditions")
                                uc.postconditions.push(condition);

                            // if no user or system actors are specified, act as a new condition per line
                            condition = null;
                        }
                    }


                    break;

                case "scenario":
                case "extensions":

                    if (uc === null) break;

                    if (section == "scenario"   && section_line_no === 1) {
                        uc.scenario = new Scenario();
                        continue;
                    }
                    if (section == "extensions" && section_line_no === 1)
                        continue;

                    if (regexps.step_begin().test(line)) {
                        var orig_name = line,
                            step_no = regexps.step_begin().exec(line).toString(),
                            step_name = line.replace(regexps.step_begin(), '').replace(/^[\s]*/, '');

                        // inherited
                        var inherited = false;
                        if (regexps.step_inherited().test(step_name)) {
                            step_name = step_name.replace(regexps.step_inherited(), '');
                            inherited = true;
                        }

                        /*
                         * Check if there is a new step by users
                         * if user variable is not null, there is a new step by the user
                         */
                        var u,
                            user = null;
                        for (var a in uc.actors) {
                            u = a;
                            if (utils.startsWith(step_name, u))
                                user = u;
                        }

                        // check if there is a new trigger by system
                        for (var i in this.system_actors) {
                            u = this.system_actors[i];
                            if (utils.startsWith(step_name, u))
                                user = u;
                        }

                        if (user !== null) {
                            // new step by actor

                            // remove user from step_name
                            var t_name = args.extract(step_name.replace(new RegExp('^' + user + "\\s+"), ''));

                            if (!regexps.condition_begin().test(line))
                                step = new Step(t_name['name']);
                            else
                                step = new Condition(t_name['name']);

                            step.orig_name = orig_name;
                            step.no = step_no;
                            step.args = step.args.concat(t_name['args']);
                            step.played_by = user;
                            step.inherited = inherited;

                            uc.scenario.set_step(step_no, step);
                        } else {
                            var t_name = args.extract(step_name);

                            // trigger without actor
                            if (!regexps.condition_begin().test(line))
                                step = new Step(t_name['name']);
                            else
                                step = new Condition(t_name['name']);
                            step.orig_name = orig_name;
                            step.no = step_no;
                            step.args = step.args.concat(t_name['args']);
                            step.inherited = inherited;

                            uc.scenario.set_step(step_no, step);

                        }
                    } else {

                        // existing step written in multiple lines
                        if (step !== null) {
                            var orig_name = line,
                                t_name = args.extract(line);
                            step.name =
                                cleanName(this.append(t_name['name'], step.name));
                            // step.orig_name+=orig_name;
                            step.args = step.args.concat(t_name['args']);
                        }
                    }

                    break;
            }

        }
        // if (uc && uc.triggers && uc.triggers) {
        //     console.log('Trigger');
        //     console.log(uc.triggers);
        // }
        // if (uc && uc.extension_points && uc.extension_points) {
        //     console.log('Extension point');
        //     console.log(uc.extension_points);
        // }
        return uc;

    };

    this.parseFiles = function (path) {

        var data = fs.readFileSync(path, 'utf8'),
            match,
            files = [],
            comments = regexps.comments(),

            File = function (path, body, tags, raw_body) {
                this.path = path;
                this.body = body;
                this.tags = tags;
                this.raw_body = raw_body;
            };

        /*
         * Iterate through comments
         */
        while ((match = comments.exec(data)) !== null) {

            var lines = (match && match.length >= 1) ? match[0].split("\n") : [],
                line,
                body_start = false, fpath = null, body = '', tags = [];

            for (var i in lines) {
                line = lines[i];

                if (body_start) {

                    // last line contains *
                    if (lines.length-1 <= i)
                        line = line.replace(regexps.comments_trash(), '');

                    body += line+"\n";

                } else {
                    line = line.replace(regexps.comments_trash(), '');

                    // remove empty lines
                    if (/^\s*$/.test(line)) continue;

                    // skip UC
                    if (regexps.uc_begin().test(line)) break;

                    // test if code
                    if (!regexps.file_begin().test(line))
                        break;

                    fpath = line
                        .replace(regexps.file_begin(), '');

                    tags = fpath.match(regexps.file_tag());
                    if (tags !== null) {
                        fpath = fpath
                            .replace(regexps.file_tag(), '');
                        fpath = fpath
                            .replace(/^\s*/, '').replace(/\s*$/, '');
                    } else {
                        tags = [];
                    }

                    body_start = true;
                }

            }

            if (fpath !== null)
                files.push(new File(fpath, body, tags, match[0]));

        }

        return files;
    };

    this.append = function (line, text) {
        if (text === null)
            text = line;
        else
            text += this.line_separator+line;
        return text;
    };

    this.get_section = function(line) {
        var i, section, section_regexp;
        for (i in this.sections) {
            section = this.sections[i];
            section_regexp = regexps.sections[section]();
            if (section_regexp.test(line)) {
                return section;
            }
        }
        return false;
    };
};

