
var log = console.log.bind(console),
    regexps = require('../regexps.js');

module.exports = {
    analyse: function (code, lang) {
        if ((!(lang in regexps)) || regexps[lang].ignore) {
            return [];
        }
        var scopes_match = code.match(regexps[lang].scope_names()),
            scopes = [],
            /*
             * mapping between regexp match and analyse of body {}
             */
            scopes_mapping = {},
            cls_body = code,
            char,

            parent_scopes_body = {},
            started_scopes_body = [],
            scopes_body = [],
            started_ignore_scopes = [],

            Scope = function () {

                this.so_far_matched = "";
                this.states = [];

                /**
                 * State machine
                 */
                this.position = 0;
                this.isThisScope = function (char) {
                    var will_it_match = this.so_far_matched + char;

                    if (this.states[this.position]().test(will_it_match)) {

                        this.position++;
                        this.so_far_matched = will_it_match;

                    } else {
                        this.position = 0;
                        this.so_far_matched = "";
                    }

                    if (this.states.length === this.position) {

                        this.position = 0;
                        this.so_far_matched = "";

                        return true;
                    } else {
                        return false;
                    }
                };
            },
            scopes_start = [],
            scopes_end = [],

            Cls = function (signature) {
                this.type = null;
                this.name = null;
                this.lang = null;
                this.signature = signature;
                this.childs_i = [];
                this.childs = [];
                this.body = "";
                this.file_body = "";

                /**
                 * State machine
                 */
                this.position = 0;
                this.isThisScope = function (char) {
                    if (this.signature[this.position] === char) {
                        this.position++;
                    } else {
                        this.position = 0;
                    }
                    if (this.signature.length === this.position) {
                        return true;
                    } else {
                        return false;
                    }
                };
            },
            Method = Cls;

        for (var s in regexps[lang].scope) {
            var reg_scope = regexps[lang].scope[s];
            var sc = new Scope();
            sc.states = reg_scope.start;
            scopes_start.push(sc);

            var sc = new Scope();
            sc.states = reg_scope.end;
            scopes_end.push(sc);
        }

        for (var i in scopes_match) {
            var scope_header = scopes_match[i];

            if (regexps[lang].is_cls().test(scope_header)) {

                var cls = new Cls(scope_header);
                cls.name = scope_header.match(regexps[lang].is_cls())[1];
                cls.type = 'cls';
                cls.file_body = cls_body;
                cls.lang = lang;
                scopes.push(cls);

            } else if (regexps[lang].is_attr().test(scope_header)) {

                var attr = new Method(scope_header);
                attr.name = scope_header.match(regexps[lang].is_attr())[1];
                attr.type = 'attr';
                attr.file_body = cls_body;
                attr.lang = lang;
                scopes.push(attr);

            } else if (regexps[lang].is_method().test(scope_header)) {

                var method = new Method(scope_header);
                method.name = scope_header.match(regexps[lang].is_method())[1];
                method.type = 'method';
                method.file_body = cls_body;
                method.lang = lang;
                scopes.push(method);

            } else {
                //log ('unknown scope', scope_header);
            }

        }

        for (var i = 0, len = cls_body.length; i < len; i++) {
            char = cls_body[i];

            for (var j in regexps[lang].ignore_scope_inside) {
                var start = regexps[lang].ignore_scope_inside[j][0],
                    end = regexps[lang].ignore_scope_inside[j][1],

                    start_chars = "",
                    end_chars = "";
                for (var k = 0, l = start.length; k < l; k++)
                    start_chars += cls_body[i+k];
                for (var k = 0, l = end.length; k < l; k++)
                    end_chars += cls_body[i+k];

                if ((start !== end && start_chars === start)   ||
                        (start === end && start_chars === start && started_ignore_scopes.indexOf(start) === -1)) {
                    started_ignore_scopes.push(start);
                } else if (end_chars === end && started_ignore_scopes.indexOf(start) !== -1) {
                    started_ignore_scopes.splice(started_ignore_scopes.indexOf(start), 1);
                }
            }

            for (var j in started_scopes_body) {
                scopes_body[started_scopes_body[j]] += char;
            }

            for (var j in scopes_start) {
                if (scopes_start[j].isThisScope(char)) {
                    if (started_ignore_scopes.length === 0) {
                        scopes_body.push("");
                        parent_scopes_body[scopes_body.length-1] = started_scopes_body.slice(0);

                        started_scopes_body.push(scopes_body.length-1);
                    }
                }
            }

            // start SCOPE
            //if (started_ignore_scopes.length === 0 &&
            //        char === regexps[lang].scope_start) {
            //
            //    scopes_body.push("");
            //    parent_scopes_body[scopes_body.length-1] = started_scopes_body.slice(0);
            //
            //    started_scopes_body.push(scopes_body.length-1);
            //}


            for (var j in scopes) {
                if (scopes[j].isThisScope(char)) {
                    // map last
                    if (scopes[j].type === 'attr') {

                        // simulate start SCOPE
                        scopes_body.push("");
                        parent_scopes_body[scopes_body.length-1] = started_scopes_body.slice(0);

                        started_scopes_body.push(scopes_body.length-1);

                        // DO map
                        scopes_mapping[j] = started_scopes_body[started_scopes_body.length-1];

                        // simulate END SCOPE
                        started_scopes_body.pop();

                    } else
                        scopes_mapping[j] = started_scopes_body[started_scopes_body.length-1];
                }
            }

            for (var j in scopes_end) {
                if (scopes_end[j].isThisScope(char)) {
                    if (started_ignore_scopes.length === 0) {

                        started_scopes_body.pop();
                    }
                }
            }

            // end SCOPE
            //if (started_ignore_scopes.length === 0 &&
            //        char === regexps[lang].scope_end) {
            //
            //    started_scopes_body.pop();
            //}


        }


        for (var i in scopes_mapping) {
            var scope_i = i,
                scope_body_i = scopes_mapping[i],
                scope_parents = parent_scopes_body[scope_body_i];

            scopes[scope_i].body = scopes_body[scope_body_i];

            // iterujem cez parentov
            for (var j in scope_parents) {
                var parent = scope_parents[j];
                // ci sa nenachadza v mappingu parrent
                for (var k in scopes_mapping) {
                    var p_scope_i = k,
                        p_scope_body_i = scopes_mapping[k];
                    // ak ma parenta, tak do neho vlozim akoby child i
                    if (parent === p_scope_body_i) {
                        scopes[p_scope_i].childs_i.push(scope_i);
                    }
                }
            }
        }

        function child_in(childs, to_be_checked, scopes) {

            for (var i in childs) {
                for (var j in to_be_checked) {
                    // ma tam moje dieta
                    if (scopes[to_be_checked[j]].childs_i.indexOf(childs[i]) !== -1) {
                        // zmazem ho
                        childs.splice(childs.indexOf(childs[i]), 1);

                        // rekurzivne pozriem dalsie
                        childs = child_in(childs, scopes[to_be_checked[j]].childs_i.slice(0), scopes);
                    }
                }
            }
            return childs;
        }

        function remove_sub_childs (scopes) {
            for (var i in scopes) {
                scopes[i].childs_i = child_in(scopes[i].childs_i, scopes[i].childs_i.slice(0), scopes);
            }
            return scopes;
        }

        scopes = remove_sub_childs(scopes);

        function get_top_most (scopes) {
            var top_most_scopes = [];
            for (var i in scopes) {
                var is_child = false;
                for (var j in scopes) {
                    if (scopes[j].childs_i.indexOf(i) > -1) {
                        is_child = true;
                        break;
                    }
                }
                if (!is_child) {
                    top_most_scopes.push(i);
                }
            }
            return top_most_scopes;
        }
        var top_most_scopes = get_top_most(scopes);


        function add_childs (scope, scopes) {

            var j = 0;
            while (scope.childs_i.length > scope.childs.length) {

                scope.childs.push(
                    add_childs(scopes[scope.childs_i[j]], scopes)
                );

                j++;
            }
            return scope;
        }

        function build_class_tree (top_most_scopes, scopes) {

            for (var i in top_most_scopes) {
                top_most_scopes[i] = add_childs(scopes[top_most_scopes[i]], scopes);
            }
            return top_most_scopes;
        }

        var tree = build_class_tree(top_most_scopes, scopes);

        // for (var j in tree) {
        //     var tree_item = tree[j];
        //     this.print_tree(tree_item, 0);
        // }
        

        return tree;
    },
    print_tree: function (tree_item, level) {

        if (tree_item) {
            var indent = "";
            for (var i = 0; i < level; i++) {
                indent += " ";
            }
            indent += " \\-";
            console.log(indent+" "+tree_item.name+" - "+tree_item.type);

            if (tree_item.childs) {
                for (var i in tree_item.childs) {
                    this.print_tree(tree_item.childs[i], level+1);
                }
            }
        }
    }
};
