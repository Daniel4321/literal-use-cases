var log = console.log.bind(console),
    regexps = require('../regexps.js'),
	acorn = require('acorn');
	jsjavaparser = require('javaparser7');
var tree;

module.exports = {
		analyse: function (code,lang){
		switch(lang){
			case "javascript":
				tree = acorn.parse(code);
				console.log(tree);
			case "java":
				tree = jsjavaparser.parse(code);
				console.log(tree);			
		}	
	}/*
print_tree: function (tree_item, level) {

        if (tree_item) {
            var indent = "";
            for (var i = 0; i < level; i++) {
                indent += " ";
            }
            indent += " \\-";
            console.log(indent+" "+tree_item.name+" - "+tree_item.type);

            if (tree_item.childs) {
                for (var i in tree_item.childs) {
                    this.print_tree(tree_item.childs[i], level+1);
                }
            }
        }
    }	*/

};
