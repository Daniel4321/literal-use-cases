

var regexps = require('./regexps');

module.exports = {
    format: function (lang, code) {

        var lines = code.split(regexps[lang].separator),
            line,
            char,
            last_level = 0,
            level = 0,
            spaces = 4,
            spaces_str,
            formatted_code = "",

            Scope = function () {

                this.so_far_matched = "";
                this.states = [];

                /**
                 * State machine
                 */
                this.position = 0;
                this.isThisScope = function (char) {
                    var will_it_match = this.so_far_matched + char;

                    if (this.states[this.position]().test(will_it_match)) {

                        this.position++;
                        this.so_far_matched = will_it_match;

                    } else {
                        this.position = 0;
                        this.so_far_matched = "";
                    }

                    if (this.states.length === this.position) {

                        this.position = 0;
                        this.so_far_matched = "";

                        return true;
                    } else {
                        return false;
                    }
                };
            },
            scopes_start = [],
            scopes_end = [],

            shift;


        for (var s in regexps[lang].scope) {
            var reg_scope = regexps[lang].scope[s];
            var sc = new Scope();
            sc.states = reg_scope.start;
            scopes_start.push(sc);

            var sc = new Scope();
            sc.states = reg_scope.end;
            scopes_end.push(sc);
        }


        for (var i in lines) {
            line = lines[i].replace(/^\s*/, '');


            last_level = JSON.parse(JSON.stringify(level));

            for (var j = 0, len = line.length; j < len; j++) {
                char = line[j];

                for (var l in scopes_start) {
                    if (scopes_start[l].isThisScope(char)) {
                        level++;
                    }
                }

                for (var l in scopes_end) {
                    if (scopes_end[l].isThisScope(char)) {
                        level--;
                    }
                }
            }


            if (level === last_level) {
                shift = (spaces*level);
            } else if (level > last_level) {
                shift = (spaces*last_level);
            } else if (level < last_level) {
                shift = (spaces*level);
            }

            spaces_str = "";
            for (var k = 0; k < shift; k++) {
                spaces_str += " ";
            }
            line = spaces_str + line + regexps[lang].separator;

            if (line)
                formatted_code += line;


        }

        return formatted_code;
    }
};