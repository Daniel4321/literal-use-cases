

module.exports = {
    Usecase:    require('./parsers/usecase'),
    code:       require('./parsers/code'),
    formatter:  require('./formatter')
};