module.exports = {
    comments:           wrap( /\*([^*]|[\r\n]|(\*+([^*/]|[\r\n])))*\*/g ),
    comments_trash:     wrap( /^[\s\*=-]+|[\s\*]+$/ ),

    sections: {
        usecase:        wrap( /^Usecase.*$/   ),
        actors:         wrap( /^Actors$/    ),
        triggers:       wrap( /^Triggers$/  ),
        scenario:       wrap( /^Main success scenario$/ ),
        extensions:     wrap( /^Extensions$/ ),
        preconditions:  wrap( /^Pre-conditions$/        ),
        postconditions: wrap( /^Post-conditions$/       ),
        extension_points: wrap(/^Extension points$/     )
    },

    uc_begin:           wrap( /^Usecase[\s]+/       ),
    uc_specializes:     wrap( /specializes\s+([a-zA-Z0-9\ ,]+)\s*$/       ),
    step_begin:         wrap( /^[0-9a-zA-Z]+\./     ),
    condition_begin:    wrap( /^[0-9a-zA-Z]+[a-zA-Z]\./    ),
    step_inherited:     wrap( /^\s*\(inherited\)\s*/ ),
    include:            wrap( /^Include/ ),
    extension_point:    "extension point",

    actor_name:         wrap( /^[0-9a-zA-Z\s]+:/    ),

    between_quotes:     wrap( /(["'])(\\?.)*?\1/g   ),

    file_begin:         wrap( /^Partial[\s]+/       ),
    file_tag:           wrap( /(@[a-z\d-]+)/g  ),


    java:  {

        /**
         * Get all signatures of classes, methods and attributes
         * ... containing scope start
         */
        scope_names: wrap( /^(.+[\r\t\f ]+[a-zA-Z0-9]+)[\s\(\)a-zA-Z0-9,=\.]+[{;]/gm ),

        /**
         * Differ classes, methods and attributes
         * ... containing scope start
         */
        is_cls:      wrap( /class[a-zA-Z0-9\s]*\s+([a-zA-Z0-9]+)[\r\t\f ]+{/m ),
        is_attr:     wrap( /[\r\t\f ]*[a-zA-Z0-9_]+[\r\t\f ]+([a-zA-Z0-9_]+)[a-zA-Z0-9_=\r\t\f \.]*;/m ),
        is_method:   wrap( /[\r\t\f ]*([a-zA-Z0-9_]+)[\r\t\f ]*\([a-zA-Z0-9,\r\t\f ]*\)\s*{/m ),

        /**
         * Separator, that separates definitions
         */
        separator: "\n",


        scope: [
            /**
             * Define states where last state is a new scope
             */
            //{
            //    start: [
            //        wrap( /d/       ),
            //        wrap( /de/      ),
            //        wrap( /def/     ),
            //        wrap( /def.*/   ),
            //        wrap( /def.*\n/ )
            //    ],
            //    end: [
            //        wrap( /end/ )
            //    ]
            //},
            {
                start: [
                    wrap( /{/ ),
                ],
                end: [
                    wrap( /}/ )
                ]
            }
        ],

        /**
         * How to end class?
         */
        scope_end_cls: '}',

        /**
         * Mark fragment
         */
        mark_fragment: "\n\n/**\n" +
            " * %usecase%\n" +
            " */",

        ignore_scope_inside: [
            ["//", "\n"],
            ["/*", "*/"],
            ["\"", "\""]
        ],

        ignore: false
    },


    php:  {

        scope_names: wrap( /^(.+[\r\t\f ]+[a-zA-Z0-9\$\\<]+)[\?\s\\\(\)\$a-zA-Z0-9,=\.]+[{;]*/gm ),

        is_cls:      wrap( /class[a-zA-Z0-9\s\\]*\s+([a-zA-Z0-9\\]+)[\r\t\f ]+{/m ),
        is_attr:     wrap( /[\r\t\f ]*[a-zA-Z0-9_]+[\r\t\f ]+(\$[a-zA-Z0-9_]+)[a-zA-Z0-9_=\r\t\f \.]*;/m ),
        is_method:   wrap( /[\r\t\f ]*[a-zA-Z0-9_]*[\r\t\f ]*function[\r\t\f ]+([a-zA-Z0-9_]+)\([\$a-zA-Z0-9,\r\t\f ]*\)\s*{/m ),

        separator: "\n",

        scope: [
            {
                start: [
                    wrap( /{/ ),
                ],
                end: [
                    wrap( /}/ ),
                ]
            },
            {
                start: [
                    wrap( /\(/ ),
                ],
                end: [
                    wrap( /\)/ ),
                ]
            }
        ],

        scope_end_cls: '}',

        mark_fragment: "\n\n/**\n" +
        " * %usecase%\n" +
        " */",

        ignore_scope_inside: [
            ["//", "\n"],
            ["/*", "*/"],
            ["\"", "\""]
        ],

        ignore: false
    },


    /**
     * html does not contain code that controls the app
     */
    html:  {
        ignore: true
    }
};

function wrap(r) {
    return function() {
        return eval(r.toString())
    };
}
