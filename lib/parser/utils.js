module.exports = {

    camelCase: function (s) {
        return (s||'').toLowerCase().replace(/(\b|\s)\w/g, function(m) {
            return m.toUpperCase().replace(/\s/,'');
        });
    },

    underscoreCase: function (s) {
        return (s||'').toLowerCase().replace(/(\b|\s)\w/g, function(m) {
            return m.replace(/\s/,'_');
        });
    },

    startsWith: function (text, str){
        return text.indexOf(str) === 0;
    },

    parseURL: function (url) {

        var parser = document.createElement('a'),
            params = {},
            queries, split, i;

        // Let the browser do the work
        parser.href = url;

        queries = parser.search.replace(/^\?/, '').split('&');
        if (!(queries.length === 1 && queries[0] == "")) {
            for( i = 0; i < queries.length; i++ ) {
                split = queries[i].split('=');
                params[split[0]] = decodeURIComponent(split[1]);
            }
        }

        return {
            protocol: parser.protocol,
            host: parser.host,
            hostname: parser.hostname,
            port: parser.port,
            pathname: parser.pathname,
            params: params,
            hash: parser.hash
        };
    }
};
