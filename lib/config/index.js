module.exports = {

    project_id: null,

    /*
     * Sources path
     * ------------
     *
     * There are 2 kinds of sources:
     * 1. use cases
     *    - realization of use cases
     *    - define files
     * 2. templates
     *    - the main method
     */
    usecase_path: '../app/src/tpl/context',
    tpl_path: '../app/src/tpl',
    tpl_ignore_paths: [

    ],
    tpl_copy_paths: [
        //'../app/src/tpl/context',
        //'../app/src/tpl/lib'
    ],
    file_extension: 'tpl',

    /*
     * Generator path
     * --------------
     *
     * All generated files are located here
     */
    gen_path: '../app/src/lib',

    /*
     * Generator use case path
     * -----------------------
     *
     * Files defined in sources (use cases) are:
     * 1. extracted (they remains in sources) and
     * 2. generated here
     */
    //gen_usecase_path: '%gen_path%/parts/%level%/%name%',
    gen_usecase_path: '%gen_path%',

    /*
     * Generator model path
     * --------------------
     *
     * File parts with the same class name defined in sources (use cases) are:
     * 1. merged together
     * 2. extracted (they remains in sources) and
     * 3. generated here
     */
    //gen_model_path: '%gen_path%/model',
    gen_model_path: '%gen_path%',


    /*
     * Setup database
     */
    database   : {
        protocol : "sqlite",
        pathname : 'db.sqlite3'
    }
};