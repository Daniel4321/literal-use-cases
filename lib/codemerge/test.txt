types [ { node: 'TypeDeclaration',
    name: { identifier: 'Tovar', node: 'SimpleName' },
    superInterfaceTypes: [],
    superclassType: null,
    bodyDeclarations: [ [Object], [Object] ],
    typeParameters: [],
    interface: false,
    modifiers: [] },
  { node: 'TypeDeclaration',
    name: { identifier: 'Objednavka', node: 'SimpleName' },
    superInterfaceTypes: [],
    superclassType: null,
    bodyDeclarations: [ [Object], [Object] ],
    typeParameters: [],
    interface: false,
    modifiers: [] } ]
0 { node: 'TypeDeclaration',
  name: { identifier: 'Tovar', node: 'SimpleName' },
  superInterfaceTypes: [],
  superclassType: null,
  bodyDeclarations: 
   [ { node: 'FieldDeclaration',
       fragments: [Object],
       type: [Object],
       modifiers: [] },
     { node: 'FieldDeclaration',
       fragments: [Object],
       type: [Object],
       modifiers: [] } ],
  typeParameters: [],
  interface: false,
  modifiers: [] }
name { identifier: 'Tovar', node: 'SimpleName' }
superInterfaceTypes []
superclassType null
bodyDeclarations [ { node: 'FieldDeclaration',
    fragments: [ [Object] ],
    type: { node: 'SimpleType', name: [Object] },
    modifiers: [] },
  { node: 'FieldDeclaration',
    fragments: [ [Object] ],
    type: { node: 'SimpleType', name: [Object] },
    modifiers: [] } ]
0 { node: 'FieldDeclaration',
  fragments: 
   [ { node: 'VariableDeclarationFragment',
       name: [Object],
       extraDimensions: 0,
       initializer: null } ],
  type: 
   { node: 'SimpleType',
     name: { identifier: 'String', node: 'SimpleName' } },
  modifiers: [] }
fragments [ { node: 'VariableDeclarationFragment',
    name: { identifier: 'meno', node: 'SimpleName' },
    extraDimensions: 0,
    initializer: null } ]
0 { node: 'VariableDeclarationFragment',
  name: { identifier: 'meno', node: 'SimpleName' },
  extraDimensions: 0,
  initializer: null }
name { identifier: 'meno', node: 'SimpleName' }
initializer null
type { node: 'SimpleType',
  name: { identifier: 'String', node: 'SimpleName' } }
name { identifier: 'String', node: 'SimpleName' }
modifiers []
1 { node: 'FieldDeclaration',
  fragments: 
   [ { node: 'VariableDeclarationFragment',
       name: [Object],
       extraDimensions: 0,
       initializer: null } ],
  type: 
   { node: 'SimpleType',
     name: { identifier: 'Float', node: 'SimpleName' } },
  modifiers: [] }
fragments [ { node: 'VariableDeclarationFragment',
    name: { identifier: 'cena', node: 'SimpleName' },
    extraDimensions: 0,
    initializer: null } ]
0 { node: 'VariableDeclarationFragment',
  name: { identifier: 'cena', node: 'SimpleName' },
  extraDimensions: 0,
  initializer: null }
name { identifier: 'cena', node: 'SimpleName' }
initializer null
type { node: 'SimpleType',
  name: { identifier: 'Float', node: 'SimpleName' } }
name { identifier: 'Float', node: 'SimpleName' }
modifiers []
typeParameters []
modifiers []
1 { node: 'TypeDeclaration',
  name: { identifier: 'Objednavka', node: 'SimpleName' },
  superInterfaceTypes: [],
  superclassType: null,
  bodyDeclarations: 
   [ { node: 'FieldDeclaration',
       fragments: [Object],
       type: [Object],
       modifiers: [] },
     { parameters: [],
       thrownExceptions: [],
       body: [Object],
       extraDimensions: 0,
       typeParameters: [],
       node: 'MethodDeclaration',
       returnType2: [Object],
       name: [Object],
       constructor: false,
       modifiers: [] } ],
  typeParameters: [],
  interface: false,
  modifiers: [] }
name { identifier: 'Objednavka', node: 'SimpleName' }
superInterfaceTypes []
superclassType null
bodyDeclarations [ { node: 'FieldDeclaration',
    fragments: [ [Object] ],
    type: { node: 'SimpleType', name: [Object] },
    modifiers: [] },
  { parameters: [],
    thrownExceptions: [],
    body: { node: 'Block', statements: [] },
    extraDimensions: 0,
    typeParameters: [],
    node: 'MethodDeclaration',
    returnType2: { node: 'PrimitiveType', primitiveTypeCode: 'void' },
    name: { identifier: 'pridajTovar', node: 'SimpleName' },
    constructor: false,
    modifiers: [] } ]
0 { node: 'FieldDeclaration',
  fragments: 
   [ { node: 'VariableDeclarationFragment',
       name: [Object],
       extraDimensions: 0,
       initializer: null } ],
  type: 
   { node: 'SimpleType',
     name: { identifier: 'Array', node: 'SimpleName' } },
  modifiers: [] }
fragments [ { node: 'VariableDeclarationFragment',
    name: { identifier: 'zoznamTovarov', node: 'SimpleName' },
    extraDimensions: 0,
    initializer: null } ]
0 { node: 'VariableDeclarationFragment',
  name: { identifier: 'zoznamTovarov', node: 'SimpleName' },
  extraDimensions: 0,
  initializer: null }
name { identifier: 'zoznamTovarov', node: 'SimpleName' }
initializer null
type { node: 'SimpleType',
  name: { identifier: 'Array', node: 'SimpleName' } }
name { identifier: 'Array', node: 'SimpleName' }
modifiers []
1 { parameters: [],
  thrownExceptions: [],
  body: { node: 'Block', statements: [] },
  extraDimensions: 0,
  typeParameters: [],
  node: 'MethodDeclaration',
  returnType2: { node: 'PrimitiveType', primitiveTypeCode: 'void' },
  name: { identifier: 'pridajTovar', node: 'SimpleName' },
  constructor: false,
  modifiers: [] }
parameters []
thrownExceptions []
body { node: 'Block', statements: [] }
statements []
typeParameters []
returnType2 { node: 'PrimitiveType', primitiveTypeCode: 'void' }
name { identifier: 'pridajTovar', node: 'SimpleName' }
modifiers []
typeParameters []
modifiers []
package null
imports [ { node: 'ImportDeclaration',
    name: { node: 'QualifiedName', qualifier: [Object], name: [Object] },
    static: true,
    onDemand: false },
  { node: 'ImportDeclaration',
    name: { node: 'QualifiedName', qualifier: [Object], name: [Object] },
    static: false,
    onDemand: true } ]
0 { node: 'ImportDeclaration',
  name: 
   { node: 'QualifiedName',
     qualifier: { node: 'QualifiedName', qualifier: [Object], name: [Object] },
     name: { identifier: 'out', node: 'SimpleName' } },
  static: true,
  onDemand: false }
name { node: 'QualifiedName',
  qualifier: 
   { node: 'QualifiedName',
     qualifier: { node: 'QualifiedName', qualifier: [Object], name: [Object] },
     name: { identifier: 'System', node: 'SimpleName' } },
  name: { identifier: 'out', node: 'SimpleName' } }
qualifier { node: 'QualifiedName',
  qualifier: 
   { node: 'QualifiedName',
     qualifier: { identifier: 'java', node: 'SimpleName' },
     name: { identifier: 'lang', node: 'SimpleName' } },
  name: { identifier: 'System', node: 'SimpleName' } }
qualifier { node: 'QualifiedName',
  qualifier: { identifier: 'java', node: 'SimpleName' },
  name: { identifier: 'lang', node: 'SimpleName' } }
qualifier { identifier: 'java', node: 'SimpleName' }
name { identifier: 'lang', node: 'SimpleName' }
name { identifier: 'System', node: 'SimpleName' }
name { identifier: 'out', node: 'SimpleName' }
1 { node: 'ImportDeclaration',
  name: 
   { node: 'QualifiedName',
     qualifier: { identifier: 'java', node: 'SimpleName' },
     name: { identifier: 'util', node: 'SimpleName' } },
  static: false,
  onDemand: true }
name { node: 'QualifiedName',
  qualifier: { identifier: 'java', node: 'SimpleName' },
  name: { identifier: 'util', node: 'SimpleName' } }
qualifier { identifier: 'java', node: 'SimpleName' }
name { identifier: 'util', node: 'SimpleName' }
