var program = require('../parser/node_modules/jsjavaparser/lib/javaparser7.js');

var fs = require('fs');
var ast;
fs.readFile("vstup.txt", "utf8", function (err, data) {

	ast = program.parse(data);
	//console.log(ast);
	/*function traverse(o ) {
	    for (i in o) {
	        if (typeof(o[i])=="object") {
	            console.log(i, o[i])
	            console.log("DALSIE \n")
	            traverse(o[i] );
	        }
	    }
	}*/

	main(ast);
	
});

function main (ast){

	var classes = ast.types;
	//console.log(classes[0].bodyDeclarations[0]);
	console.log(getNodeString(classes[0].bodyDeclarations[0]));

}
/*

function AstFunctions (){
	this.getVariableName = function (bodyDeclaration){
		return bodyDeclaration.fragments[0].name.identifier;
	}
}

function ConvertToCode (ast){
	var classes = ast.types;


}

function getStringFromAst(codeSection){
	var code = "\n";
	var i=0;
	switch (codeSection){
		case "FieldDeclaration":
			while (codeSection.modifiers[i]){
				code.concat(modifiers[i].keyword);
			}
			code.concat(codeSection.type.name.identifier);
			if (codeSection.type.type)
			
		break;
	}
}
*/
function getNodeString(input){
	console.log(input);
	var code = " ";
	var i=0;
	switch (input.node){
		case "Assignment":
			code.concat(getNodeString(input.leftHandSide));
			code.concat(input.operator);
			code.concat(getNodeString(input.rightHandSide));
			return code;
		break;
		case "Block":
			code.concat("{\n");

			i=0;
			while (input.statements[i]){
				code.concat(input.statements[i]);
				i++;
			}
			code.concat("}\n");
			return code;

		break;

		case "ClassInstanceCreation": 		//TODO: dorobit
			code.concat("= new");
			code.concat(getNodeString(input.type));
			return code;

		break;

		case "CompilationUnit":
		break;
		case "EnhancedForStatement":
			code.concat("for (");
			code.concat(getNodeString(input.parameter));
			code.concat(":");
			code.concat(getNodeString(input.expression));
			code.concat(")");
			code.concat(getNodeString(input.body));
			return code;
		break;
		case "ExpressionStatement":
			return getNodeString(input.expression);

		break;
		case "FieldAccess":
			code.concat(getNodeString(input.expression));
			code.concat(getNodeString(input.name));
			return code;
		break;
		case "FieldDeclaration":
			// MODIFIERS START
			i=0;
			while (input.modifiers[i]){
				code.concat(getNodeString(input.modifiers[i]));
				i++;

			}
			// MODIFIERS END
			
			// TYPES START
			code.concat(getNodeString(input.type));
			// TYPES END

			// FRAGMENTS START
			i=0;
			while (input.fragments[i]){
			code.concat(getNodeString(input.fragments[i]))
			i++;
			}
			// FRAGMENTS END
			code.concat("\n");
			return code;
		break;
		case "ImportDeclaration":
		break;
		case "MethodDeclaration": 		//TODO: need to add exceptions
			// MODIFIERS START
			i=0
			while (input.modifiers[i]){
				code.concat(getNodeString(input.modifiers[i]));
				i++;
			}
			// MODIFIERS END
			code.concat(getNodeString(input.returnType2));
			code.concat(getNodeString(input.name));
			// PARAMETERS START
			code.concat("(");
			i=0
			while (input.parameters[i]){
				code.concat(getNodeString(input.parameters[i]));
				code.concat(",");
				i++;
			}
			code.concat(")");
			// PARAMETERS END

			// BODY START
			code.concat(getNodeString(input.body));
			// BODY END
			return code;

		break;
		case "MethodInvocation":
			code.concat(getNodeString(input.expression));
			code.concat(getNodeString(input.name));
			code.concat("(");
			i=0;
			while (input.arguments[i]){
				code.concat(getNodeString())
				i++;
			}
			code.concat(")");
			return code;
		break;
		case "Modifier":
			return input.keyword;
		break;
		case "ParameterizedType":
			code.concat(getNodeString(input.type));
			code.concat("<");
			if (input.typeArguments[0]){
				code.concat(getNodeString(input.typeArguments[0])); // works only for one type argument!
			}
			code.concat(">");
			return code;
		break;
		case "PrimitiveType":
			return input.primitiveTypeCode;
		break;
		case "QualifiedName":
			code.concat(getNodeString(input.qualifier));
			code.concat(".");
			code.concat(getNodeString(input.name));
			return code;
		break;
		case "ReturnStatement":
			code.concat(getNodeString(input.expression));
			return code;
		break;
		case "SimpleName":
			return input.identifier;
		break;
		case "SimpleType":
			return getNodeString(input.name);
		break;

		case "SingleVariableDeclaration":
			code.concat(getNodeString(input.type));
			code.concat(getNodeString(input.name));
			return code;
		break;
		case "ThisExpression":
			return "this.";
		break;
		case "TypeDeclaration":
			// MODIFIERS START
			i=0;
			while (input.modifiers[i]){
				code.concat(getNodeString(input.modifiers[i]));
				i++;

			}
			// MODIFIERS END
			code.concat(" class");
			code.concat(getNodeString(input.name));
		break;
		case "VariableDeclarationFragment":
			code.concat(getNodeString(input.name));
			if (input.initializer != null){
				code.concat(getNodeString(input.initializer));
			}
			return code;
		break;
		case "VariableDeclarationStatement":
			// MODIFIERS START
			i=0;
			while (input.modifiers[i]){
				code.concat(getNodeString(input.modifiers[i]));
				i++;

			}
			// MODIFIERS END
			
			// TYPES START
			code.concat(getNodeString(input.type));
			// TYPES END

			// FRAGMENTS START
			i=0;
			while (input.fragments[i]){
			code.concat(getNodeString(input.fragments[i]))
			i++;
			}
			// FRAGMENTS END
			code.concat("\n");
			return code;
		break;
	}
}