(function (metadata) {

    this.metadata = metadata;
    this.state = null;
    this.after_cbs = [];

    this.after = function (cb) {
        this.after_cbs.push(cb);
    }

    /**
     * Load this
     */
    this.load = function (args, done) {

        var _this = this;

        get(this.metadata.path, function (impl) {
            _this.extend(eval(impl));
            _this.state = 'init';
            _this.bind(args, function () {
                done(_this);
            });
        });
    };

    this.bind = function (args, done) {
        for (var i in this.metadata.triggers) {
            var trigger = this.metadata.triggers[i],
                method_name = this.camelCase(trigger.name.replace(/\./, '')),
                args = trigger.args.push(args),
                method = this[method_name];

            if (method !== undefined) {
                args.push(function () {
                    done();
                });
                method.apply(this, args);
            }
        }

        // if (this.metadata.scenario.steps["1."].played_by in this.metadata.actors) {
            this.continue(args, function () {
                done();
            });
        // }
    };

    this.continue = function (args, done) {

        var prevStep = this.state, 
            nextStep = "1.",
            _this = this;
        
        if (this.state !== "init") {
            var nextStep = this.nextStep(this.state);
        }

        console.log(nextStep);

        //extend
        if (nextStep in this.metadata.extension_points) {

            // var real_done = done;
            // done = function () {
                load(_this.metadata.extension_points[nextStep], args, function (usecase) {
                    usecase.after(function () {
                        // real_done();
                        done();
                    });
                    usecase.continue([], function () {});
                });
            // }
            return;
        }
        if (nextStep in this.metadata.scenario.steps) {

            if (this.metadata.scenario.steps[nextStep].name == "Include") {
                // include
                load(this.metadata.scenario.steps[nextStep].args[0], args, function (usecase) {
                        usecase.after(function () {
                            done();
                        });
                        // usecase.continue([], function () {});
                    });
            } else {
                return this.call(nextStep, args, done);
            }
        } else {

            for (var i in this.after_cbs) {
                this.after_cbs[i]();
            }

            // no more steps, check post-conditions
            return this.callPost(args, done);
        }
        
    };

    this.call = function (step, args, done) {

        var _this = this,
            step = this.metadata.scenario.steps[step],
            methodName = this.lowerCaseFirstLetter(this.camelCase(step.name)),
            method = this[methodName];

        if (step.args.length >= 1) {
            args.push(step.args);
        }

        args.push(function () {
            done();
        });
        this.state = step.no;

        // if function call it!
        if (typeof this[methodName] == 'function') {
            method.apply(this, args);
        } else {
            //else if it is not a function, try load layer below
            var doneLower = function () {
                done();
                _this.continue([], function () {});
            }
            console.log('Loading lower layer '+step.name, args);
            load(step.name, args, function (usecase) {
                usecase.after(function () {
                    doneLower();
                });
                // usecase.continue(args, function () {});
            });
        }
    };

    this.callPost = function (args, done) {

        for (var i in this.metadata.postconditions) {
            var postcnd = this.metadata.postconditions[i],
                postcndMethodName = this.lowerCaseFirstLetter(this.camelCase(postcnd.name)),
                postcndMethod = this[postcndMethodName];

            args.push(function () {
                done();
            });

            postcndMethod.apply(this, args);
        }
    };

    this.nextStep = function (step) {

        var last = step.match(/([0-9]+)\.$/)[0],
            next = parseInt(last) + 1,
            prevPart = step.substring(0, step.length - last.length);

        next = prevPart + next.toString() + '.';

        return next;
    };

    this.camelCase = function (s) {
        s=s.replace(/\./, '').replace(/\,/, '');
        return (s||'').toLowerCase().replace(/(\b|\s)\w/g, function(m) {
            return m.toUpperCase().replace(/\s/,'');
        });
    };

    this.lowerCaseFirstLetter = function (s) {
        return s.charAt(0).toLowerCase() + s.slice(1);
    };

    this.extend = function (Function) {
        var object = new Function();
        for (var property in object) {
            if (typeof this[property] === 'undefined') {
                this[property] = object[property];
            }
        }
    };

});