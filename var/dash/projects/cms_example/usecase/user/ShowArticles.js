/**
 * Usecase Show articles
 * =====================
 *
 * This UC displays articles in a list.
 *
 * Actors
 * ------
 * User: any user visiting our site
 *
 * Triggers
 * --------
 * The use case is triggered when the "saving article" extension point is reached
 *
 * Main success scenario
 * ---------------------
 * 1. User selects show articles
 * 2. System displays articles
 *
 * Post-conditions
 * ---------------
 * User can see articles.
 *
 */

(function () {

    this.selectsShowArticles = function (done) {
        var _this = this;
        document.getElementById('button-show-articles').onclick = function () {
            _this.continue([], function () {});
        };
        done();
    };

    this.displaysArticles = function (done) {

        /** Partial Article.php
         <?php

         class Article {
            public $title;
            public $content;

            public static function find() {
                return array(
                    array(
                        'title' => 'The first article',
                        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean imperdiet dictum neque, ut euismod diam ultricies nec. Donec suscipit velit vel viverra dictum. Suspendisse eu erat tempus, facilisis enim at, bibendum dui. Duis euismod viverra egestas. Nam fringilla vestibulum elementum. Pellentesque eleifend in ante sed sollicitudin.'
                    ),
                    array(
                        'title' => 'The second example of an article',
                        'content' => 'Donec sit amet pellentesque est, id sodales tortor. Curabitur id mauris elementum, blandit sapien a, pulvinar velit. Suspendisse in ornare ligula. Mauris sapien velit, tincidunt nec dui nec, hendrerit volutpat dolor. Suspendisse ultrices nibh vel elementum blandit.'
                    )
                );
            }
         }

         */


        /** Partial getArticles.php
         <?php

         require 'Article.php';
         echo json_encode(Article::find());

         */


        var _this = this;

        get(this.metadata.pathDir+'/getArticles.php', function (data) {
            var articles = JSON.parse(data);



            /** Partial @prop show-article.html
                 <h1>Article: {%=o.title%}</h1>
                 <p>{%=o.content%}</p>
         */

            /** Partial articles-list.html


             {% for (var i=0, article; article=o.articles[i]; i++) { %}
             <div class="article" data-id="{%=i%}">
                <h3>
                    {%=article.title%}
                </h3>
                <p>
                    {%=article.content%}
                </p>
             </div>
             {% } %}

             */

            get(_this.metadata.pathDir+'/articles-list.html', function (data) {

                var gen = tmpl(data, {articles: articles});
                document.getElementById('content').innerHTML = gen;

            });
        });


    };
});
