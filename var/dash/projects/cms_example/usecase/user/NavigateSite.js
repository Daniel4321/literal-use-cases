/**
 * Usecase Navigate site
 * =====================
 *
 * If user visits the site, this use case renders a frame.
 *
 * Actors
 * ------
 * User: Anyone who visited the site.
 *
 * Main success scenario
 * ---------------------
 * 1. User loads the site
 * 2. System displays frame
 * 3. System displays menu
 * 4. System displays footer
 *
 * Post-conditions
 * ---------------
 * User can see the navigation.
 *
 */


(function () {

    this.loadsTheSite = function (done) {
        this.continue([], done);
    };

    this.displaysFrame = function (done) {
      
      
        /** Partial frame.css
			.logo * {
            	text-align:center;
                margin: 10px;
            }
            #content {
            	margin: 20px 0;
            }
		*/

        /** Partial frame.html

         <link rel='stylesheet'  href="{%=o.pathDir%}/frame.css" type="text/css" />

         <div class="container">

             <div class="row logo">
                <div class="col-12">
                    <img src="/img/logo.png" alt="">
                </div>
             </div>

             <div id="menu">

             </div>

             <div id="content">
                 <p>
                    Welcome to <strong>Breaking News</strong>.
                 </p>
             </div>

             <div id="footer">

             </div>
         </div>

         */

        var _this = this;

        get(this.metadata.pathDir+'/frame.html', function (data) {
            var gen = tmpl(data, {});
            document.body.innerHTML = gen;
            done();
            _this.continue([], function () {});
        });
    };


    this.displaysMenu = function (done) {

        /** Partial menu.css

         ul {
              list-style-type: none;
              margin: 0;
              padding: 0;
              overflow: hidden;
              background-color: #333;
          }

          li {
              float: left;
          }

          li a {
              display: block;
              color: white;
              text-align: center;
              padding: 14px 16px;
              text-decoration: none;
          }

          li a:hover, li a:visited {
              color: white;
              background-color: #111;
              text-decoration: none;
          }

         */

        /** Partial menu.html

         <link rel='stylesheet'  href="{%=o.pathDir%}/menu.css" type="text/css" />

         <div class="row">
            <div class="col-12">
                 <ul id="menu">
                     <li><a href="#" id="button-add-article">Add Article</a></li>
                     <li><a href="#" id="button-show-articles">Show Articles</a></li>
                     <li><a href="#" id="button-review-articles">Review Articles</a></li>
                     <li><a href="#" id="button-place-order">Place Order</a></li>
                 </ul>
            </div>
         </div>

         */

        var _this = this;

        get(this.metadata.pathDir+'/menu.html', function (data) {
            var gen = tmpl(data, {
                pathDir: _this.metadata.pathDir
            });
            document.getElementById('menu').innerHTML = gen;

            load('Show articles', [], function () {
                load('Add article', [], function () {
                    load('Place order', [], function () {
                        done();

                        _this.continue([], function () {});
                    });
                });
            });

        });
    };


    this.displaysFooter = function (done) {

        /** Partial footer.css
        	.footer {
              	color: #333;
              	background-color: #efefef;
                height: 48px;
                line-height: 48px;
            }
            .footer p {
            	margin: 0 15px;
            }
         */

        /** Partial footer.html

         <link rel='stylesheet'  href="{%=o.pathDir%}/footer.css" type="text/css" />
         
         <div class="row footer">
            <div class="col-12">
            	<p>2016 Breaking News.</p>
         	</div>
         </div>

         */
 
        var _this = this;

        get(this.metadata.pathDir+'/footer.html', function (data) {
            var gen = tmpl(data, {});
            document.getElementById('footer').innerHTML = gen;
            done();
            _this.continue([], function () {});
        });
    };

    this.canSeeTheNavigation = function () {
        if (!document.getElementById('menu')) {
            throw "User cannot see the navigation !";
        }
    };
});
