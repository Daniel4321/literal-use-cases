/**
 *
 * Usecase Place order
 * ===================
 *
 * Placing order
 *
 * Actors
 * ------
 * User:
 * Guest: Anyone who visits site can order
 * Member: Registered user with at least one order in past 
 * 
 * Pre-conditions
 * --------------
 * User has selected items to be purchased.
 *
 * Post-conditions
 * ---------------
 * Order will be placed in the system.
 *
 * Main success scenario
 * ---------------------
 * 1. User selects to order items.
 * 2. System prompts for "Billing info", "Shipping info".
 * 3. User enters billing and shipping info.
 * 4. System shows the price including taxes, shipping charges and estimated delivery date.
 * 5. User confirms the price.
 * 6. System prompts for method of payment.
 * 7. User selects a method of payment.
 * 8. System requests Billing system for charging user for the order.
 * 9. Billing system confirms that user was charged.
 * 10. System notifies user that was charged.
 *
 * Extensions
 * ----------
 * 9a. Billing system rejects payment.
 * 9a1. System notifies user that payment has failed.
 * 9a2. Use case returns to step 6.
 *
 * 
 */

(function () {

    this.selectsToOrderItems = function (done) {
        var _this = this;
        document.getElementById('button-place-order').onclick = function () {
            _this.continue([], function () {});
        };
        done();
    };

    this.entersBillingAndShippingInfo = function (done) {

      document.getElementById('submit').onclick = function () {
        _this.continue([], function () {});
      };
    };













































































});