<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DizP</title>

    <link href="lib/css/bootstrap.min.css" rel="stylesheet">

    <script src="lib/js/tmpl.min.js"></script>
    <script src="main.js"></script>

    <!--[if lt IE 9]>
    <script src="lib/js/html5shiv.min.js"></script>
    <script src="lib/js/respond.min.js"></script>
    <![endif]-->
</head>
<body onload="main('Navigate site')">

</body>
</html>