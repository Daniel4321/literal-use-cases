



<?php

class Article {
    
    
    
    /**
    * Fragment Add article
    */
    
    public $createdAt;
    
    public $publishedAt;
    
    public $title;
    
    public $content;
    
    public function save() {
        // do save
    }
    
    
    /**
    * Fragment Review article
    */
    
    public static function getById($id) {
        
        if ($id === "1") {
            return array(
                'id' => 1,
                'title' => 'The first article',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean imperdiet dictum neque, ut euismod diam ultricies nec. Donec suscipit velit vel viverra dictum. Suspendisse eu erat tempus, facilisis enim at, bibendum dui. Duis euismod viverra egestas. Nam fringilla vestibulum elementum. Pellentesque eleifend in ante sed sollicitudin.',
                'published' => false, 
                'note' => null,  
                'save' =>  function () {
                    return true; 
                }  
            );
        } else
        
        if ($id === "2") {    
            return  array(
                'id' => 2,
                'title' => 'The second example of an article',
                'content' => 'Donec sit amet pellentesque est, id sodales tortor. Curabitur id mauris elementum, blandit sapien a, pulvinar velit. Suspendisse in ornare ligula. Mauris sapien velit, tincidunt nec dui nec, hendrerit volutpat dolor. Suspendisse ultrices nibh vel elementum blandit.',
                'published' => false, 
                'note' => null ,
                'save' =>  function () { 
                    return true; 
                }   
            );
        }
        
    }
    
    public static function find() {
        return array(
            array(
                'title' => 'The first article',
                'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean imperdiet dictum neque, ut euismod diam ultricies nec. Donec suscipit velit vel viverra dictum. Suspendisse eu erat tempus, facilisis enim at, bibendum dui. Duis euismod viverra egestas. Nam fringilla vestibulum elementum. Pellentesque eleifend in ante sed sollicitudin.'
            ),
            array(
                'title' => 'The second example of an article',
                'content' => 'Donec sit amet pellentesque est, id sodales tortor. Curabitur id mauris elementum, blandit sapien a, pulvinar velit. Suspendisse in ornare ligula. Mauris sapien velit, tincidunt nec dui nec, hendrerit volutpat dolor. Suspendisse ultrices nibh vel elementum blandit.'
            )
        );
    }
}



