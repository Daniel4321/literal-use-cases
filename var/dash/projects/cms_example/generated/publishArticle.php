          <?php
          include 'User.php';
          include 'Article.php';
          include 'Review.php';

          if (!in_array("reviewer", User::get()["permissions"])) {
          exit("Not allowed");
      }

          $article = Article::getById($_POST['id']);
          $article['published'] = true;
          
            $review = new Review();
          $review->note = $_POST['note'];
          $article['review'] = $review;

          echo json_encode(array(
           'error' => !$article['save']()
          ));


