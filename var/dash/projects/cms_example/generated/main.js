
/**
 * In desktop app, get would be replaced with import
 */
window.get = function (path, done) {
    var file = new XMLHttpRequest();
    file.open("GET", path+"?r="+((new Date()).getTime()), true);
    file.onreadystatechange = function () {
        if(file.readyState === 4 && (file.status === 200 || file.status == 0)) {
            done(file.responseText);
        }
    };
    file.send(null);
};

window.post = function (path, data, done) {
    var file = new XMLHttpRequest();
    file.open("POST", path, true);
    file.onreadystatechange = function () {
        if(file.readyState === 4 && (file.status === 200 || file.status == 0)) {
            done(file.responseText);
        }
    };
    var str = "";
    for (var key in data) {
        if (str != "") {
            str += "&";
        }
        str += key + "=" + encodeURIComponent(data[key]);
    }
    file.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    file.send(str);
};


function main (init) {
    get('usecase.js', function (usecase) {
        var Usecase = eval(usecase);

        get('usecases.json', function (usecasesMetadata) {
            usecasesMetadata = JSON.parse(usecasesMetadata);


            window.load = function (name, args, done) {
                for (var i in usecasesMetadata.usecases) {
                    var usecaseMetadata = usecasesMetadata.usecases[i];

                    if (usecaseMetadata['name'].toLowerCase() == name.toLowerCase()) {
                        console.log("loading ", name, args);
                        var usecase = new Usecase(usecaseMetadata);

                        usecase.load(args, function (usecase) {
                            done(usecase);
                            // Done!
                        });
                        break;
                    }
                }
            };

            load(init, [], function () {
                // init loaded
            })
        });
    });
}

