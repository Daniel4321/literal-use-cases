/**
 * Usecase Review article
 * ======================
 *
 * Actors
 * ------
 * Author: Writes article.
 *
 * Actors
 * ------
 * Reviewer: Reviews article
 * Author: Writes article
 *
 * Main success scenario
 * ---------------------
 * 1. Reviewer selects a article for review
 * 2. System displays the article
 * 3. System allows to reviewer to publish and delete the article
 * 4. Reviewer selects publish article
 * 5. System prompts for note
 * 6. Reviewer enters note
 * 7. Reviewer selects submit button
 * 8. System saves the review
 *
 * Extensions
 * ----------
 * 6a. Author does not enter title and description
 * 6a1. System notify about required fields
 * 6a2. System returns to step 3
 *
 * Post-conditions
 * ---------------
 * User can see the article.
 *
 * Extension points
 * ----------------
 * "finding article": step "3."
 *
 */


(function (uc) {

    this.selectsAArticleForReview = function (done) {
        var _this = this;

        var list = document.
            getElementsByClassName("article");
        for (var i = 0; i < list.length; i++) {
            var title = list[i];
            title.onclick = function (ev) {
                _this.continue([title], function () {});
            }
        }
        done();
    };

    this.displaysTheArticle = function (title) {
        var _this = this;


        /** Partial Article.php
         <?php

         class Article { 
         
            public static function getById($id) {
            
                if ($id === "1") {
                    return array(
                        'id' => 1,
                        'title' => 'The first article',
                        'content' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean imperdiet dictum neque, ut euismod diam ultricies nec. Donec suscipit velit vel viverra dictum. Suspendisse eu erat tempus, facilisis enim at, bibendum dui. Duis euismod viverra egestas. Nam fringilla vestibulum elementum. Pellentesque eleifend in ante sed sollicitudin.',
                        'published' => false, 
                        'note' => null,  
                        'save' =>  function () {
                            return true; 
                        }  
                    );
                } else
                
                if ($id === "2") {    
                    return  array(
                        'id' => 2,
                        'title' => 'The second example of an article',
                        'content' => 'Donec sit amet pellentesque est, id sodales tortor. Curabitur id mauris elementum, blandit sapien a, pulvinar velit. Suspendisse in ornare ligula. Mauris sapien velit, tincidunt nec dui nec, hendrerit volutpat dolor. Suspendisse ultrices nibh vel elementum blandit.',
                        'published' => false, 
                        'note' => null ,
                        'save' =>  function () { 
                            return true; 
                        }   
                    );
                }
                
            }
         }

         */
      
       /** Partial User.php
        <?php
        
        class User {
         static function get()  {
               return array(
                'permissions' => array( 'reviewer' )
               );
            }
        }
        */

        /** Partial getArticle.php
         <?php
         include 'User.php';
         include 'Article.php';

         echo json_encode(array(
          'article' => Article::getById($_GET['id']),
          'user' => User::get()
         ));

         */

        /** Partial article-detail.html
        
         <div id="article" data-id="{%=o.id%}">
         <h1>Article: {%=o.title%}</h1>
         <p>{%=o.content%}</p>
         <div id="controls"></div>
         </div>

         */


        get(_this.metadata.pathDir+'/getArticle.php?id='+title.getAttribute('data-id'), function (data) {
            data = JSON.parse(data);

            get(_this.metadata.pathDir+'/article-detail.html', function (s) {

                var gen = tmpl(s, {
                    id: data.article.id,
                    title: data.article.title,
                    content: data.article.content
                });
                document.getElementById('content').innerHTML = gen;
                _this.continue([data.user]);
            });

        });
    };

    this.allowsToReviewerToPublishAndDeleteTheArticle = function (user) {
        var _this = this;
        if (user.permissions.indexOf("reviewer") !== -1) {
            document.getElementById("controls").innerHTML += "<button id='publish-article'>Publish article</button>";

            document.getElementById("publish-article").onclick = function () {
                _this.continue([], function () {});
            }
        }
    };


    this.selectsPublishArticle = function () {
        var _this = this;


        /** Partial review-form.html
         <div class="note">
         <p>Enter note</p>
         <div><textarea id="note"></textarea></div>
         <div><button id="note-submit">Submit</button></div>
         </div>
         */

        get(_this.metadata.pathDir+'/review-form.html', function (data) {
            var gen = tmpl(data, {});
            document.getElementById('content').innerHTML += gen;

            _this.continue([], function () {});
        });
    };
    this.promptsForNote = function () {this.continue([], function () {});};
    this.entersNote = function () {this.continue([], function () {});};
    this.selectsSubmitButton = function () {

        var _this = this;
        document.getElementById('note-submit').onclick = function () {
            console.log('a');
            _this.continue([], function () {});
        };
    };

    this.savesTheReview = function () {

      
       /** Partial Review.php
        <?php
        
        class Review {
         public $note;
        }
        */
      
        /** Partial @sync publishArticle.php
          <?php
          include 'User.php';
          include 'Article.php';
          include 'Review.php';

          if (!in_array("reviewer", User::get()["permissions"])) {
          exit("Not allowed");
      }

          $article = Article::getById($_POST['id']);
          $article['published'] = true;
          
            $review = new Review();
          $review->note = $_POST['note'];
          $article['review'] = $review;

          echo json_encode(array(
           'error' => !$article['save']()
          ));

         */

        var _this = this;
        post(_this.metadata.pathDir+"/publishArticle.php", {
            id: document.getElementById('article').getAttribute('data-id'),
            note: document.getElementById('note').value
        }, function (result) {
            result = JSON.parse(result);
            if (!result.error) {
                document.getElementById('content').innerHTML = "Successfully saved!";
            }
        });
    };


});


