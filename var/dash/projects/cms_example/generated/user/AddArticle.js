/**
 * Usecase Add article
 * ===================
 *
 * A user adds a new article into the database.
 *
 * Actors
 * ------
 * Author: Writes article.
 *
 * Main success scenario
 * ---------------------
 * 1. Author selects add article
 * 2. System prompts for title and content
 * 3. Author writes an article
 * 4. Author enters title and description
 * 5. Author selects submit button
 * 6. System saves the article
 *
 * Extensions
 * ----------
 * 6a. Author does not enter title and description
 * 6a1. System notify about required fields
 * 6a2. System returns to step 3
 *
 * Post-conditions
 * ---------------
 * User can see the article.
 *
 * Extension points
 * ----------------
 * "saving article": step "6."  
 *
 */

 
(function (uc) { 

    this.selectsAddArticle = function (done) {
        var _this = this;
        document.getElementById('button-add-article').onclick = function () {
            _this.continue([], function () {});
        };
        done();
    };

    this.promptsForTitleAndContent = function (done) {

        /** Partial add-article-form.html
         <form>
             <div class="form-group">
                 <label for="title">Title</label>
                 <input type="text" class="form-control" id="article-title" placeholder="Enter title">
             </div>
             <div class="form-group">
                 <label for="content">Content</label>
                 <textarea class="form-control" id="article-content" rows="3"></textarea>
             </div>
             <div id="result"></div>
             <button type="submit" class="btn btn-default" id="do-add-article">Submit</button>
         </form>
         */

        var _this = this;

        get(_this.metadata.pathDir+'/add-article-form.html', function (data) {

            var gen = tmpl(data, {});
            document.getElementById('content').innerHTML = gen;
            done();
            _this.continue([], function () { });
        });
    };

    this.writesAnArticle = function (done) {
        done();
        this.continue([], function () { });
    };

    this.entersTitleAndDescription = function () {
        done();
        this.continue([], function () { });
    };

    this.selectsSubmitButton = function (done) {
        var _this = this;
        document.getElementById('do-add-article').onclick = function () {
            _this.continue([], function () {});
        };
        done();
    };

    this.savesTheArticle = function (done) {

        /** Partial Article.php

        <?php

        class Article {
            public $createdAt;
            public $publishedAt;
            public $title;
            public $content;

            public function save() {
                    // do save
            }
         }

         */


         /** Partial saveArticle.php
         <?php

          require 'Article.php';

            $article = new Article();
            $article->title = 'POST[title]';
            $article->content = 'POST[content]';
            $article->createdAt = "2015-04-30 12:10:00";
            $article->save();

            echo '{"err":false}';

        */

                 
        /** Partial @prop publishArticle.php
          <?php
          include 'User.php';
          include 'Article.php';
          include 'Review.php';

          if (!in_array("reviewer", User::get()["permissions"])) {
          exit("Not allowed");
      }

          $article = Article::getById($_POST['id']);
          $article['published'] = true;
          
            $review = new Review();
          $review->note = $_POST['note'];
          $article['review'] = $review;

          echo json_encode(array(
           'error' => !$article['save']()
          ));

         */


          get(_this.metadata.pathDir+'/saveArticle.php', function (result) {
                    result = JSON.parse(result);


                    /** Partial success.html
                     <div class="alert alert-success" role="alert">Successfully saved</div>
                     */

                    if (!result.err) {
                        get(_this.metadata.pathDir+'/success.html', function (data) {
                            var gen = tmpl(data, {});
                            document.getElementById('result').innerHTML = gen;
                        });
                    } else {
                        document.getElementById('result').innerHTML =
                            '<div class="alert alert-danger" role="alert">'+err+'</div>';
                    }

                    done();
                    _this.continue([
                        document.getElementById('article-title').value,
                        document.getElementById('article-content').value
                    ], function () {});
           });


    };

    this.displaysTheArticle = function (title, content, done) {


        /** Partial @prop show-article.html
                 <h1>Article: {%=o.title%}</h1>
                 <p>{%=o.content%}</p>
         */

        var _this = this;

        get(_this.metadata.pathDir+'/show-article.html', function (data) {

            var gen = tmpl(data, {
                title: title,
                content: content
            });
            document.getElementById('content').innerHTML += gen;
            done();
        });

    };


    this.userCanSeeTheArticle = function (done) {
        return true;
    };

});
