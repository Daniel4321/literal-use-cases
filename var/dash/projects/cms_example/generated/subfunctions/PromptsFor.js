 /*
 * Usecase Prompts for
 * ===================
 *
 * This UC describes rendering a form.
 *
 * Actors
 * ------
 * None
 *
 * Main success scenario
 * ---------------------
 * 1. System renders a form based on arguments
 *    "title": "input"
 *    "description": "textarea"
 *    "group": "input"
 *    "note": "textarea"
 *    "submit": "button"
 *    "Shipping info": "textarea"
 *    "Billing info": "textarea"
 *
 */


(function () {
    this.rendersAFormBasedOnArguments = function (argsToUsecase, done, stepArgs, doneThis) {

     var _this = this;

     document.getElementById('content').innerHTML = "";

     for (var i in argsToUsecase) {
      var name = argsToUsecase[i];
      for (var j in stepArgs) {
       if (name == stepArgs[j]) {
        var type = stepArgs[++j];

        switch(type) {
         case "input":

            /** Partial input.html
                <div class="form-group">
                    <label for="title">{%=o.name%}</label>
                    <input type="text" class="form-control" id="input-{%=o.name%}" placeholder="Enter {%=o.name%}">
                </div>
             */
         break;

         case "textarea":

            /** Partial textarea.html
                <div class="form-group">
                    <label for="content">{%=o.name%}</label>
                    <textarea class="form-control" id="textarea-{%=o.name%}" rows="3"></textarea>
                </div>
             */
         break;
        }

        function render (name, type) {
               get(_this.metadata.pathDir+'/'+type+'.html', function (data) {

                   var gen = tmpl(data, {name: name});
                   document.getElementById('content').innerHTML += gen;
               });
              }
              render(name, type);

        break;
       }
      }
     }

     // submit by default

  /** Partial submit.html
         <div id="result"></div>
         <button type="submit" class="btn btn-default" id="submit">Submit</button>
  */

  get(_this.metadata.pathDir+'/submit.html', function (data) {
   var gen = tmpl(data, {});
   document.getElementById('content').innerHTML += gen;


         doneThis();
         done();
         _this.continue([], function () {});
  });
    };

});
