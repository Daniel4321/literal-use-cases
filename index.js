
var cfg = require('_/config'),
    app = require('_/app'),
    Project = require('_/app/project'),
    web = require('_/web'),
    model = require('_/app/model')(function (err, db) {

        app.models = db.models;
        app.db     = db;

        if (err)
            throw err;

	
        // username, project_name
        var args = process.argv.slice(2);
        if (args.length >= 2) {
            var project = new Project(app, app.db);
            project.getAll(args[0], function (projects) {
                for (var i in projects) {
                    if (projects[i]['name'] === args[1]) {
                        console.log("Opening project "+projects[i]['name']);
                        project.open(projects[i]['id'], function (result) {

                        });
                    }
                }
            });
        }


        var server = web(app, db).listen(3000, function () {
            var host = server.address().address;
            var port = server.address().port;

            console.log('Example app listening at http://%s:%s', host, port);
        });
    });
